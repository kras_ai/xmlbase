unit XmlBase.DataBase.Intf;

interface

uses
  System.SysUtils, System.Types;

type
  TItemKind = (dbiXML,dbiZIP);
  TItemKinds = set of TItemKind;

IDataBase = Interface
  ['{0EE63CEB-68E6-4DAF-9D99-AF4341365E30}']
    function GetTempdirectory:WideString;
    function GetDirectory:WideString;
    function GetFinedItems:TStringDynArray;
    function GetKinds:TItemKinds;

    procedure SetDestDir(const Value: WideString);
    function GetDestDir:WideString;

    function ShowInformation(FileName:WideString):WideString;
    procedure AddKind(const Value:TItemKind);

    procedure FindbyNumList(CadNums:WideString);
    procedure FindByMaskList(const aMaskList:WideString);
    function GetNoFined: TStringDynArray;

    function Load(const aFileName: WideString; log: TStringBuilder = nil): boolean;

    procedure UpLoad(OnlyActual: boolean = true);overload;
    procedure UpLoad(I:integer; OnlyActual: boolean = true);overload;
    procedure UpLoadMask(I:integer; OnlyActual: boolean = true);

    property TempDirecroty:WideString read GetTempdirectory;
    property Directory:WideString read GetDirectory;
    property DestDirectory:WideString read GetDestDir write SetDestDir;
    property FinedItems:TStringDynArray read GetFinedItems;
    property NoFined: TStringDynArray read GetNoFined;
    property Kind:TItemKinds read GetKinds;
End;
implementation

end.
