unit XmlBase.Common;

interface
  uses
   System.SysUtils,
   System.RegularExpressions,
   XML.xmldom, XML.XMLDoc, XML.XMLIntf,

   MsXmlApi,
   XmlBase.XmlClasses,
   SurveyingPlan.Common;

  type
   Common = record
   public class var
    XSDFile: string;
    XSLTFile: string;
   public
    class function isValid(aFileName: string): boolean; static;
    class function doTransform(aSourceFileName,aDestFileName: string): boolean; static;

    class function getDate(aNode: IXMLTXmlItem):string; static;
    class function getName(aNode: IXMLTXmlItem): string; static;
    class function getNameEx(aNode: IXMLTXmlItem; Item: TCadNumItemsKind): string; static;
    class function getFileName(aNode: IXMLTXmlItem): string; static;
    class function getPath(aNode: IXMLTXmlItem): string; overload; static;
    class function getPath(aCadastralNumber: string): string; overload; static;
   end;

implementation

{ Common }

class function Common.getFileName(aNode: IXMLTXmlItem): string;
begin
 Result:= Common.getName(aNode)+' '+Common.getDate(aNode)+'.xml';
end;

class function Common.getPath(aNode: IXMLTXmlItem): string;
begin
 Result:= Common.getPath(aNode.Name);
end;

class function Common.doTransform(aSourceFileName, aDestFileName: string): boolean;
begin
 result:=  MsXml_DOMTransform(aSourceFileName,common.XSLTFile,aDestFileName);
end;

class function Common.getDate(aNode: IXMLTXmlItem): string;
var
 frmt: TFormatSettings;
 dfrmt: TFormatSettings;
begin
 frmt.DateSeparator := '-';
 frmt.ShortDateFormat := 'yyyy-mm-dd';
 dfrmt.DateSeparator := '-';
 dfrmt.ShortDateFormat := 'dd-mm-yyyy';
 Result:= DateToStr(StrToDate(aNode.Date,frmt),dfrmt);
 //result:= StringReplace(Result,'.',' ',[rfReplaceAll, rfIgnoreCase]);
end;

class function Common.getName(aNode: IXMLTXmlItem): string;
begin
 Result:= StringReplace(aNode.Name,':',' ',[rfReplaceAll, rfIgnoreCase]);
end;

class function Common.getNameEx(aNode: IXMLTXmlItem; Item: TCadNumItemsKind): string;
var
 _math: TMatch;
begin
 Result:= '';
 _math:= TRegEx.Match(aNode.Name,cnRegExCadastralNumber);
 if _math.Success then
  TRY
   Result:= _math.Groups.Item[cnCadNumItems[Item]].Value;
  EXCEPT
   Result:= '';
  END;

end;

class function Common.getPath(aCadastralNumber: string): string;
var
 _math: TMatch;
 _sb: TStringBuilder;
begin
 Result:= '';
 _math:= TRegEx.Match(aCadastralNumber,cnRegExCadastralNumber);
 _sb:= TStringBuilder.Create;
 if _math.Success then
  TRY
   _sb.Append(_math.Groups.Item[cnCadNumItems[cnDistrict]].Value).Append('\');
   _sb.Append(_math.Groups.Item[cnCadNumItems[cnDistrict]].Value).Append(' ');
   _sb.Append(_math.Groups.Item[cnCadNumItems[cnMunicipality]].Value).Append('\');
   _sb.Append(_math.Groups.Item[cnCadNumItems[cnDistrict]].Value).Append(' ');
   _sb.Append(_math.Groups.Item[cnCadNumItems[cnMunicipality]].Value).Append(' ');
   _sb.Append(_math.Groups.Item[cnCadNumItems[cnBlock]].Value).Append('\');

   if TRegEx.IsMatch(aCadastralNumber,cnRegCadastralNumber) then begin
    _sb.Append(_math.Groups.Item[cnCadNumItems[cnDistrict]].Value).Append(' ');
    _sb.Append(_math.Groups.Item[cnCadNumItems[cnMunicipality]].Value).Append(' ');
    _sb.Append(_math.Groups.Item[cnCadNumItems[cnBlock]].Value).Append(' ');
    _sb.Append(_math.Groups.Item[cnCadNumItems[cnCadNum]].Value).Append('\');
   end;
  EXCEPT
   Result:= '';
  END;
  Result:= _sb.ToString;
 _sb.Free;
end;

class function Common.isValid(aFileName: string): boolean;
begin
 Result:= MsXml_DOMCheck(aFileName,common.XSDFile);
end;

end.
