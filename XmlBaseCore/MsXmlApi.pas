unit MsXmlApi;

interface
 uses
  Windows, Classes,XMLDoc, XMLIntf, SysUtils, Variants, MSXML2_TLB;

function MsXml_ReadAttribute(const Node: IXMLNode; AttributeName: string): string;
function MsXml_RemoveAttribute(const Node: IXMLNode; AttributeName: string): boolean;
function MsXml_ReadChildNodeValue(const Node: IXMLNode; ChildName:string): string;
function MsXml_RemoveChildNode(Node: IXMLNode; ChildName:string; AllFind: boolean = false): Boolean;

//����������� ���������-��������  �� Source � Dest. ������������ � Dest ���������
//result = 0 �� ������� ����������� ���������� �������
function MsXml_CopyChildItem(const Source,Dest: IXMLNode; ItemName: string): integer;

function MsXml_DOMCheck(const FileXml,FileXsd: string; aErrors: TStrings = nil; const aNamespace: string = ''): boolean;
function MsXml_DOMTransform(const InFile,XsltFile,OutFile:string; WithPriample: boolean=false): Boolean; overload;
function MsXml_DOMTransform(const InFile,XsltFile,OutFile:string; const WithPriample: boolean; const aEncoding: TEncoding): Boolean; overload;
function MsXml_DOMTransform_old(InFile,XsltFile,OutFile:string): Boolean;
function BpXml_DOMTransformXmlByXSLT_(PathXML,PathXSLT,ResXml:OleVariant): Boolean;

implementation


function BpXml_DOMTransformXmlByXSLT_(PathXML,PathXSLT,ResXml:OleVariant): Boolean;
var
 xslt: IXSLTemplate;
 xslDoc,xmlDoc,xmlOut: FreeThreadedDOMDocument60;
// myErr: IXMLDOMParseError;
 xslProc: IXSLProcessor;
begin
 result:= false;
 xslt:= CoXSLTemplate60.Create;
 xslDoc:= CoFreeThreadedDOMDocument60.Create;
 xslDoc.async:= false;
 xslDoc.load(PathXSLT);
 if (xslDoc.parseError.errorCode = 0) then begin
   xslt.stylesheet:= xslDoc;
   xmlDoc:= CoFreeThreadedDOMDocument60.Create;
   xmlDoc.async:= false;
   xmlDoc.load(PathXML);
   if (xmlDoc.parseError.errorCode = 0) then begin
      xslProc:= xslt.createProcessor;
      xslProc.input:= xmlDoc;
      xmlOut:= CoFreeThreadedDOMDocument60.Create;
      xslProc.output:= xmlOut;
      xslProc.transform;
      xslProc:= nil;
      xmlOut.save(ResXml);
   end;
 end;
end;



function MsXml_ReadAttribute(const Node: IXMLNode; AttributeName: string): string;
var
 _node: IXMLNode;
begin
 _node:= Node.AttributeNodes.FindNode(AttributeName);
 if (_node=nil) or (_node.NodeValue=Null) then Result:= '' else Result:= _node.NodeValue
end;

function MsXml_RemoveAttribute(const Node: IXMLNode; AttributeName: string): boolean;
var
 _node: IXMLNode;
begin
 _node:= node.AttributeNodes.FindNode(AttributeName);
 if _node<>nil then begin
  Node.AttributeNodes.Remove(_node);
  _node:= nil;
  Result:= True;
 end else
  Result:= False;
end;

function MsXml_ReadChildNodeValue(const Node: IXMLNode; ChildName:string): string;
var
 _node: IXMLNode;
begin
 _node:= Node.ChildNodes.FindNode(ChildName);
 if (_node=nil) or (_node.NodeValue=Null) then Result:= '' else Result:= _node.NodeValue
end;

function MsXml_RemoveChildNode(Node: IXMLNode; ChildName:string; AllFind: boolean = false): Boolean;
var
 _node: IXMLNode;
begin
Result:= false;
repeat
 _node:= node.ChildNodes.FindNode(ChildName);
 if _node<>nil then begin
  Node.ChildNodes.Remove(_node);
  _node:= nil;
  Result:= True;
 end;
until (AllFind=false) or (_node=nil);

end;


function MsXml_CopyChildItem(const Source,Dest: IXMLNode; ItemName: string): integer;
var
 _i: integer;
 _ChildItem: IXMLNode;
begin
Result:= 0;
TRY
 //������� ��� ��������� � dest
 while True do begin
  _ChildItem:= Dest.ChildNodes.FindNode(ItemName);
  if _ChildItem<>nil then begin
    Dest.ChildNodes.Remove(_ChildItem);
    _ChildItem:= nil;
  end else
   BREAK;
 end;

 //�������� ��������� � source
 for _i := 0 to Source.ChildNodes.Count - 1 do begin
  _ChildItem:= Source.ChildNodes.Get(_i);
  if CompareText(_ChildItem.NodeName,ItemName) =0 then
   Dest.ChildNodes.Add(_ChildItem.CloneNode(TRUE));
 end;
EXCEPT
 Result:= 1;
END;
end;

function MsXml_DOMCheck(const FileXml,FileXsd: string; aErrors: TStrings=nil; const aNamespace: string = ''): boolean;
var
 _i: Integer;
 aXML: IXMLDOMDocument3;
 SchCache: IXMLDOMSchemaCollection;
 PEC: IXMLDOMParseErrorCollection;
 tt,Path: OleVariant;
// xmlErr: string;
// XsdFilePart: PChar;
// XsdBuffer: array[0..MAX_PATH] of Char;
begin
 Path := FileXsd;
 SchCache := nil;
 SchCache := CoXMLSchemaCache60.Create;
 SchCache.add(aNamespace, Path);

 aXML:= CoDOMDocument60.Create;
 aXML.Schemas := SchCache;
 Path := FileXml;
 aXML.async:= False;
 aXML.validateOnParse:= True;
 aXML.SetProperty('NewParser', True);
 aXML.SetProperty('MultipleErrorMessages', True);
 aXML.Load(Path);
 Result := aXML.parseError.errorCode = 0;
 if (not Result) and Assigned(aErrors) then begin
  PEC:=  IXMLDOMParseError2(aXML.parseError).allErrors;
  for _i := 0 to pec.length-1 do begin
   tt:= pec.item[_i];
   aErrors.Add(tt.reason+sLineBreak+tt.srcText);
  end;
 end;
end;

function MsXml_DOMTransform(const InFile,XsltFile,OutFile:string; WithPriample: boolean): Boolean;
begin
 Result:= MsXml_DOMTransform(InFile,XsltFile,OutFile,WithPriample,TEncoding.UTF8);
end;

function MsXml_DOMTransform(const InFile,XsltFile,OutFile:string; const WithPriample: boolean; const aEncoding: TEncoding): Boolean;
var
 _SourceXmlDoc,_SourceXsltDoc: IXMLDOMDocument3;
 _Buffer, _Preamble: TBytes;
 _Stream: TFileStream;
begin
 Result:= False;
 _SourceXmlDoc:= CoDOMDocument60.Create;
 _SourceXmlDoc.async:= false;
 _SourceXmlDoc.validateOnParse:= false;
 _SourceXmlDoc.SetProperty('NewParser', True);
 _SourceXmlDoc.SetProperty('MultipleErrorMessages', True);
 _SourceXmlDoc.load(InFile);
 if _SourceXmlDoc.parseError.errorCode<>0 then
  raise Exception.Create('������ � ��������� ����� "'+InFile+'"'+sLineBreak+
                          _SourceXmlDoc.parseError.reason);

 _SourceXsltDoc:= CoDOMDocument60.Create;
 _SourceXsltDoc.async:=false;
 _SourceXsltDoc.validateOnParse:= false;
 _SourceXsltDoc.SetProperty('NewParser', True);
 _SourceXsltDoc.SetProperty('MultipleErrorMessages', True);
 _SourceXsltDoc.load(XsltFile);
 if _SourceXsltDoc.parseError.errorCode<>0 then
  raise Exception.Create('������ � ��������� ����� "'+XsltFile+'"'+sLineBreak+
                          _SourceXsltDoc.parseError.reason);

  _Stream := TFileStream.Create(OutFile, fmCreate);
 Try
  _Buffer:= aEncoding.GetBytes(_SourceXmlDoc.transformNode(_SourceXsltDoc));
  if WithPriample then begin
   _Preamble:= aEncoding.GetPreamble;
   if Length(_Preamble) > 0 then
    _Stream.WriteBuffer(_Preamble[0], Length(_Preamble));
  end;
  _Stream.WriteBuffer(_Buffer[0], Length(_Buffer));
 Finally
  _Stream.Free;
 End;
 Result:= true;
end;

function MsXml_DOMTransform_old(InFile,XsltFile,OutFile:string): Boolean;
var
 _SourceXmlDoc,_SourceXsltDoc,_DestXmlDoc: IXMLDOMDocument3;
begin
 Result:= False;
 _SourceXmlDoc:= CoDOMDocument60.Create;
 _SourceXmlDoc.async:= false;
 _SourceXmlDoc.validateOnParse:= false;
 _SourceXmlDoc.SetProperty('NewParser', True);
 _SourceXmlDoc.SetProperty('MultipleErrorMessages', True);
 _SourceXmlDoc.load(InFile);
 if _SourceXmlDoc.parseError.errorCode<>0 then
  raise Exception.Create('������ � ��������� ����� "'+InFile+'"'+sLineBreak+
                          _SourceXmlDoc.parseError.reason);

 _SourceXsltDoc:= CoDOMDocument60.Create;
 _SourceXsltDoc.async:=false;
 _SourceXsltDoc.validateOnParse:= false;
 _SourceXsltDoc.SetProperty('NewParser', True);
 _SourceXsltDoc.SetProperty('MultipleErrorMessages', True);
 _SourceXsltDoc.load(XsltFile);
 if _SourceXsltDoc.parseError.errorCode<>0 then
  raise Exception.Create('������ � ��������� ����� "'+XsltFile+'"'+sLineBreak+
                          _SourceXsltDoc.parseError.reason);

 _DestXmlDoc:= CoDOMDocument60.Create;
 _DestXmlDoc.async:= false;
 _DestXmlDoc.validateOnParse:= false;
 _DestXmlDoc.SetProperty('NewParser', True);
 _DestXmlDoc.SetProperty('MultipleErrorMessages', True);

 _SourceXmlDoc.transformNodeToObject(_SourceXsltDoc,_DestXmlDoc);
  if _DestXmlDoc.parseError.errorCode<>0 then
  raise Exception.Create('������ � ���������� �������������!!!'+sLineBreak+
                          _DestXmlDoc.parseError.reason);
 _DestXmlDoc.save(OutFile);

 Result:= true;
end;

end.
