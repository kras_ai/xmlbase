unit XmlBase.List;


{$define test_IMsDsUserList}

interface

uses
  System.Classes,
  System.Generics.Defaults,
  System.Generics.Collections;

type

 IMsDSEnumerator<T> = interface
  function getCurrent: T;
  function MoveNext: Boolean;
  procedure Reset;
  property Current: T read GetCurrent;
 end;

 IMsDSEnumerable<T> = interface
  function GetEnumerator:IMsDSEnumerator<T>;
 end;



 IMsDsList<T> = interface(IMsDSEnumerable<T>)
  function getCount: integer;
  function getItem(aIndex: integer): T;
  procedure Add(aItem: T);
  procedure Insert(Index: Integer; const Value: T);
  function Remove(const Value: T): Integer;
  function Extract(const Value: T): T;

  procedure Exchange(Index1, Index2: Integer);
  procedure Move(CurIndex, NewIndex: Integer);

  function First: T;
  function Last: T;

  procedure Clear;

  function Contains(const Value: T): Boolean;
  function IndexOf(const Value: T): Integer;

  procedure Reverse;

  procedure Sort(const AComparer: IComparer<T>);

  function ToArray: TArray<T>;

  property Item[index: integer]: T read getitem;
  property Count: integer read getCount;
 end;

 TMsDSEnumerator<T> = class(TInterfacedObject,IMsDSEnumerator<T>)
  private
   FList: TList<T>;
   FIndex: Integer;
  public
   constructor Create(aList:TList<T>);

   function getCurrent: T;
   function MoveNext: Boolean;
   procedure Reset;
 end;

 TMsDSEnumerable<T> = class(TInterfacedObject,IMsDSEnumerable<T>)
 private
  FList: TList<T>;
 public
  constructor Create;
  destructor Destroy; override;

  function GetEnumerator:IMsDSEnumerator<T>;

  property List:TList<T> read FList;
 end;

 TMsDsList<T> = class(TMsDSEnumerable<T>,IMsDsList<T>)
  function getCount: integer;
  function getItem(aIndex: integer): T;
  procedure Add(aItem: T);
  procedure Insert(Index: Integer; const Value: T);
  function Remove(const Value: T): Integer;
  function Extract(const Value: T): T;

  procedure Exchange(Index1, Index2: Integer);
  procedure Move(CurIndex, NewIndex: Integer);

  function First: T;
  function Last: T;

  procedure Clear;

  function Contains(const Value: T): Boolean;
  function IndexOf(const Value: T): Integer;

  procedure Reverse;

  procedure Sort(const AComparer: IComparer<T>);

  function ToArray: TArray<T>;
 end;


implementation

{ TMsDSEnumerator<T> }

constructor TMsDSEnumerator<T>.Create(aList: TList<T>);
begin
 self.FList:= aList;
 self.FIndex:= -1;
end;

function TMsDSEnumerator<T>.getCurrent: T;
begin
 Result := FList[FIndex];
end;

function TMsDSEnumerator<T>.MoveNext: Boolean;
begin
  if FIndex >= FList.Count then
    Exit(False);
  Inc(FIndex);
  Result := FIndex < FList.Count;
end;

procedure TMsDSEnumerator<T>.Reset;
begin
 self.FIndex:= 0;
end;

{ TMsDSEnumerable<T> }

constructor TMsDSEnumerable<T>.Create;
begin
 self.FList:= TList<T>.Create;
end;

destructor TMsDSEnumerable<T>.Destroy;
begin
 self.FList.Free;
  inherited;
end;

function TMsDSEnumerable<T>.GetEnumerator: IMsDSEnumerator<T>;
begin

 Result:= TMsDSEnumerator<T>.Create(self.FList);
end;

{ TMsDsList<T> }

procedure TMsDsList<T>.Add(aItem: T);
begin
 self.FList.Add(aItem);
end;

procedure TMsDsList<T>.Clear;
begin
 self.FList.Clear;
end;

function TMsDsList<T>.Contains(const Value: T): Boolean;
begin
 Result:= self.FList.Contains(Value);
end;

procedure TMsDsList<T>.Exchange(Index1, Index2: Integer);
begin
 self.FList.Exchange(Index1,Index2);
end;

function TMsDsList<T>.Extract(const Value: T): T;
begin
  Result:= self.FList.Extract(Value);
end;

function TMsDsList<T>.First: T;
begin
 Result:= self.FList.First;
end;

function TMsDsList<T>.getCount: integer;
begin
 Result:= self.FList.Count;
end;

function TMsDsList<T>.getItem(aIndex: integer): T;
begin
  Result:= self.FList.Items[aIndex];
end;

function TMsDsList<T>.IndexOf(const Value: T): Integer;
begin
 Result:= self.FList.IndexOf(Value);
end;

procedure TMsDsList<T>.Insert(Index: Integer; const Value: T);
begin
 self.FList.Insert(Index,value);
end;

function TMsDsList<T>.Last: T;
begin
 Result:= self.FList.last;
end;

procedure TMsDsList<T>.Move(CurIndex, NewIndex: Integer);
begin
 self.FList.Move(CurIndex,NewIndex);
end;

function TMsDsList<T>.Remove(const Value: T): Integer;
begin
  Result:= self.FList.Remove(Value);
end;

procedure TMsDsList<T>.Reverse;
begin
 self.FList.Reverse;
end;

procedure TMsDsList<T>.Sort(const AComparer: IComparer<T>);
begin
 self.FList.Sort(AComparer);
end;

function TMsDsList<T>.ToArray: TArray<T>;
begin
 Result:= self.FList.ToArray;
end;

end.
