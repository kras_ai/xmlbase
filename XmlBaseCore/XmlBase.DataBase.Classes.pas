unit XmlBase.DataBase.Classes;

interface
 uses
  System.Types,
  System.IOUtils,
  System.SysUtils,
  System.Classes,
  System.Regularexpressions,
  System.IniFiles,
  Vcl.StdCtrls,
  MsStringParser,
  SevenZip,
  XmlBase.DataBase.Intf;

type

TDataBase = class(TInterfacedObject, IDataBase)
  private
    FDirectory: WideString;
    FTempDirectory: WideString;
    FFormat:TFormatSettings;
    FFinedItems:TStringDynArray;
    FNoFined:TStringDynArray;
    FDestDir:WideString;
    FKinds:TItemKinds;

    function GetTempdirectory:WideString;
    function GetDirectory:WideString;
    function GetFinedItems:TStringDynArray;
    function GetKinds:TItemKinds;

    procedure SetDestDir(const Value: WideString);
    function GetDestDir:WideString;

    function FindByMask(const aMask: WideString): TStringDynArray;
    function FindByCadNum(const aCadNum: WideString):TStringDynArray;

    procedure UpLoadFiles(const aItems: TStringDynArray; aDestDir: string; OnlyActual: boolean = true);
    procedure UpLoadDirectory(const aItem: string; aDestDir: string; OnlyActual: boolean = true);

    function LoadZipEx(const aFileName, aTempDirectory, aDirectory: string; log: TStringBuilder = nil): boolean;
    function LoadXMLEx(const aFileName, aTempDirectory, aDirectory: string; log: TStringBuilder = nil): boolean;
    function TransformXML(const aXMLs: TArray<String>; aTempDirectory, aDirectory: string): boolean;
    function GetNoFined: TStringDynArray;
  public
    constructor Create(aPath:WideString);
    destructor Destroy; override;
    function ShowInformation(FileName: WideString):WideString;
    procedure AddKind(const Value:TItemKind);

    procedure FindbyNumList(CadNums: WideString);
    procedure FindByMaskList(const aMaskList: WideString);

    function Load(const aFileName: WideString; log: TStringBuilder = nil): boolean;

    procedure UpLoad(OnlyActual: boolean = true);overload;
    procedure UpLoad(I:integer; OnlyActual: boolean = true);overload;
    procedure UpLoadMask(I:integer; OnlyActual: boolean = true);

    property TempDirecroty:WideString read GetTempDirectory;
    property Directory:WideString read GetDirectory;
    property DestDirectory:WideString read FDestDir write SetDestDir;
    property FinedItems:TStringDynArray read GetFinedItems;
    property NoFined: TStringDynArray read GetNoFined;
    property Kind:TItemKinds read GetKinds;
  end;


 // ��������� ����� �� ������
//procedure ExtractFiles(FileName, TempDir, DestDir:string);


implementation

uses XmlBase.Common, XmlBase.XmlClasses;
 resourcestring
   rsMaskXML = '*.xml';
   rsMaskZIP = '*.zip';

{ DataBase }

constructor TDataBase.Create(aPath: WideString);
var
  _Ini: TIniFile;
  _Value: string;
begin
 _Ini:= TIniFile.Create(aPath+TPath.DirectorySeparatorChar+'XmlBase.ini');

 _Value:= _Ini.ReadString('XMLBase','XSLTFile','');
 if _Value='' then begin
  _Value:= 'XMLBase.xsl';
  _Ini.WriteString('XMLBase','XSLTFile',_Value);
 end;
 Common.XSLTFile:= aPath+TPath.DirectorySeparatorChar+_Value;

 _Value:= _Ini.ReadString('XMLBase','XSDFile','');
 if _Value='' then begin
  _Value:= 'XMLBase.xsd';
  _Ini.WriteString('XMLBase','XSDFile',_Value);
 end;
 Common.XSDFile:= aPath+TPath.DirectorySeparatorChar+_Value;

 _Value:= _Ini.ReadString('XMLBase','Directory','');
  if _Value='' then begin
  _Value:= 'T:\XmlBase';
  _Ini.WriteString('XMLBase','Directory',_Value);
 end;
 self.FDirectory:= _Value;
 _Ini.Free;

 self.FTempDirectory:= TPath.GetTempPath+'temp';
 self.FFormat:=TFormatSettings.Create;
 self.FFormat.DateSeparator:= '-';
 self.FFormat.ShortDateFormat:= 'dd-mm-yyyy';

 self.FKinds:=[];
end;

destructor TDataBase.Destroy;
begin
  inherited;
end;
// �����
function TDataBase.FindByCadNum(const aCadNum: WideString): TStringDynArray;
var
  _stb:TstringBuilder;
  _st: string;
  _i,_j:integer;
  _str:TMSStringParser;
  _sda:  TStringDynArray;
begin
  _str:=TMSStringParser.Create(' ;:');
  _stb:=TStringBuilder.Create;
  _str.Parse(aCadNum);
  for _i := 0 to _str.GetCountToken-1 do begin
   for _j := 0 to _i do begin
    _stb.Append(_str.GetToken(_j));
    if _j<>_i then
      _stb.Append(' ');
   end;
   _stb.Append(TPath.DirectorySeparatorChar);
  end;
  _str.Free;

  _st:= self.FDirectory+TPath.DirectorySeparatorChar+_stb.ToString;
  _stb.Free;

  if TDirectory.Exists(_st) then begin
    Result:=TDirectory.GetDirectories(_st, '*.*', TSearchOption.soTopDirectoryOnly);
    _sda:=TDirectory.GetFiles(_st, '*.*', TSearchOption.soTopDirectoryOnly);
    _i:= Length(Result);
    _j:= Length(_sda);
    SetLength(Result,_i+_j);
    for _j := 0 to High(_sda) do
     Result[_i+_j]:= _sda[_j];
    _sda:= nil;
  end;
end;

procedure TDataBase.FindbyNumList(CadNums: WideString);
var
  _strP:TMSStringParser;
  _k,_i,_j:integer;
  _Find:TStringDynArray;
  _st:string;
begin
  SetLength(self.FFinedItems,0);
  _strP:=TMSStringParser.Create(','+sLineBreak);
  _strP.Parse(CadNums);
  if _strP.GetCountToken<>0 then begin
   for _k := 0 to _strP.GetCountToken-1 do
   begin
    _Find:=self.FindByCadNum(_strP.GetToken(_k));
    _i:= Length(self.FFinedItems);
    _j:= Length(_Find);
    SetLength(self.FFinedItems,_i+_j);
    for _j := 0 to High(_Find) do
      self.FFinedItems[_i+_j]:=_Find[_j];
    _Find:= nil;
   end;
  end else begin
    _st:= self.FDirectory;
    if TDirectory.Exists(_st) then
      self.FFinedItems:=TDirectory.GetDirectories(_st, '*.*', TSearchOption.soTopDirectoryOnly);
  end;
end;

function TDataBase.GetDestDir: WideString;
begin
  Result:=self.FDestDir;
end;

function TDataBase.GetDirectory: WideString;
begin
  Result:=Self.FDirectory;
end;

function TDataBase.GetFinedItems: TStringDynArray;
begin
  Result:=self.FFinedItems;
end;

function TDataBase.GetKinds: TItemKinds;
begin
  Result:=self.FKinds;
end;

function TDataBase.GetNoFined: TStringDynArray;
begin
 Result:= self.FNoFined;
end;

function TDataBase.GetTempdirectory: WideString;
begin
  REsult:=self.FTempDirectory
end;

function TDataBase.LoadXMLEx(const aFileName, aTempDirectory,
  aDirectory: string; log: TStringBuilder): boolean;
var
  _FileXML: string;
begin
  Result:=True;
  TRY
    TDirectory.CreateDirectory(aTempDirectory);
    TRY
      _FileXML:=TPath.Combine(aTempDirectory, TPath.GetFileName(aFileName));
      TFile.Copy(aFileName, _FileXML);
      self.TransformXML([_FileXML], aTempDirectory, aDirectory);
    EXCEPT
      on e: Exception do Result:=False;
    END;
  FINALLY
    TDirectory.Delete(aTempDirectory,TRUE);
  END;
end;

function TDataBase.Load(const aFileName: WideString; log: TStringBuilder): boolean;
begin
  Result:=False;
  if TPath.GetExtension(aFileName).ToLowerInvariant.Equals('.zip') then
    Result:= self.LoadZipEx(aFileName,self.FTempDirectory,self.Directory)
  else
    if TPath.GetExtension(aFileName).ToLowerInvariant.Equals('.xml') then begin
      Result:= self.LoadXMLEx(aFileName,self.FTempDirectory,self.Directory);
    end;
end;

function TDataBase.LoadZipEx(const aFileName, aTempDirectory,
  aDirectory: string; log: TStringBuilder): boolean;
var
  _xmls: TArray<String>;
  _Svz: I7zInArchive;
  _FileXML, _PathName: string;
  _Files: TStringDynArray;
  _i:integer;
  _mStream: TMemoryStream;
begin
  _Svz:= CreateInArchive(CLSID_CFormatZip);
  _Svz.OpenFile(aFileName);

  //��������� ���� �� � ��������� ������ xml ���� (� �����)
  //���� ������� ������� �������� ������ ���
  _FileXML:= ''; _xmls:=[];
  _mStream:= TMemoryStream.Create;
  TRY
    for _i := 0 to _svz.NumberOfItems-1 do begin
      _mStream.Clear;
      if '.xml'= tpath.GetExtension(_svz.ItemPath[_i]).ToLowerInvariant then begin
        _Svz.ExtractItem(_i,_mStream,false);
        TDirectory.CreateDirectory(aTempDirectory);
        _FileXML:= aTempDirectory+tpath.DirectorySeparatorChar+TPath.GetFileName(_svz.ItemPath[_i]);
        _xmls:=_xmls+[_FileXML];
        _mStream.SaveToFile(_FileXML);
      end;
    end;
  FINALLY
    _mStream.Free;
  END;

  TRY
    //���� � ������ ������ ���� ������� xml
    if Length(_xmls) > 0 then begin
      self.TransformXML(_xmls, aTempDirectory, aDirectory);
      _xmls:=[];
    end else begin //���� � ������ �� ���� ������� xml �����
      _Svz.ExtractTo(aTempDirectory);
      //���� � ������������� ������ ������ ������
      _Files:= TDirectory.GetFiles(aTempDirectory,rsMaskZIP,TSearchOption.soAllDirectories);
      for _i := 0 to high (_Files) do begin
        _PathName:= TPath.Combine(TPath.GetDirectoryName(_Files[_i]), TPath.GetFileNameWithoutExtension(_Files[_i]));
        self.LoadZipEx(_Files[_i],_PathName,aDirectory,log);
      end;
    end;
    TDirectory.Delete(aTempDirectory,TRUE);
  EXCEPT
  //TDirectory.Delete(aTempDirectory,TRUE);
  END;
end;

function TDataBase.FindByMask(const aMask: WideString): TStringDynArray;
var
  _stb:TstringBuilder;
  _st: string;
  _i,_j:integer;
  _str:TMSStringParser;
  _sda:  TStringDynArray;
  _mask:string;
begin
  _str:=TMSStringParser.Create(' ;:');
  _stb:=TStringBuilder.Create;
  _str.Parse(aMask);
  if _str.GetCountToken=1 then
  begin
    _st:=_str.GetToken(0);
    _i:=Length(_str.GetToken(0));
    if _str.GetToken(0)[Length(_str.GetToken(0))]='*' then
     begin
      Delete(_st,_i,1);
     end;
     _stb.Append(Copy(_st,1,_i)).Append(TPath.DirectorySeparatorChar);
     _mask:=_str.GetToken(0);
  end
    else
  begin
    for _i := 0 to _str.GetCountToken-1 do begin
      for _j := 0 to _i do begin
        if POS('*',_str.GetToken(_i))=0 then
          _stb.Append(_str.GetToken(_j));
        if (_j<>_i) then
          _stb.Append(' ');
      end;
    if POS('*',_str.GetToken(_i))<>0 then
      BREAK
    else
      _stb.Append(TPath.DirectorySeparatorChar);
    end;

    for _i := 0 to _str.GetCountToken-1 do
      _mask:=_mask+' '+_str.GetToken(_i);
  end;


  _st:= self.FDirectory+TPath.DirectorySeparatorChar+_stb.ToString;
  _str.Free;
  _stb.Free;

  if TDirectory.Exists(_st) then begin
    _sda:=TDirectory.GetFiles(TRIM(_st), TRIM(_mask)+'*.xml', TSearchOption.soAllDirectories);
  _j:= Length(_sda);
  SetLength(Result,_j);
  for _j := 0 to High(_sda) do
    Result[_j]:= _sda[_j];
  _sda:= nil;
  end;
end;

procedure TDataBase.FindByMaskList(const aMaskList: WideString);
var
  _strP:TMSStringParser;
  _k,_i,_j:integer;
  _Find:TStringDynArray;
  _st:string;
begin
  SetLength(self.FFinedItems,0);
  self.FNoFined:= [];
  _strP:=TMSStringParser.Create(','+sLineBreak);
  _strP.Parse(aMaskList);
  if _strP.GetCountToken<>0 then begin
   for _k := 0 to _strP.GetCountToken-1 do
   begin
    _Find:=self.FindByMask(_strP.GetToken(_k));
    if Length(_Find)<>0 then
    begin
      _i:= Length(self.FFinedItems);
      _j:= Length(_Find);
      SetLength(self.FFinedItems,_i+_j);
      for _j := 0 to High(_Find) do
        self.FFinedItems[_i+_j]:=_Find[_j];
      _Find:= nil;
    end else
     self.FNoFined:= self.FNoFined+[_strP.GetToken(_k)];
   end;
  end else begin
    _st:= self.FDirectory;
    if TDirectory.Exists(_st) then
      self.FFinedItems:=TDirectory.GetDirectories(_st, '*.*', TSearchOption.soTopDirectoryOnly);
  end;
end;
// ����� ��������
procedure TDataBase.SetDestDir(const Value: WideString);
begin
  FDestDir := Value;
end;

procedure TDataBase.AddKind(const Value: TItemKind);
begin
  Include(self.FKinds, Value);
end;

function TDataBase.ShowInformation(FileName: WideString): WideString;
var
  XMLBase:IXMLXmlBase;
  _node: IXMLTXmlItem;
  INFFile:string;
begin
  Result:='';
  if TPath.GetExtension(FileName)<>'.xml' then
  begin
    Result:=TPath.GetFileName(FileName)+' �� �������� xml!';
    EXIT;
  end;
  INFFile:=TPath.GetDirectoryName(FileName)+TPath.DirectorySeparatorChar+TPath.GetFileNameWithoutExtension(FileName)+'.inf';
  if NOT TFile.Exists(INFFile) then
    Common.doTransform(FileName,INFFile);
  XMLBase:=LoadXmlBase(INFFile);
  if XMLBase.Version='' then
  begin
    TFile.Delete(INFFile);
    Common.doTransform(FileName,INFFile);
    XMLBase:=LoadXmlBase(INFFile);
  end;
  // �������� ���� ����� (���, ����, ����)
  if XMLBase.KVdocs.Count<>0 then _node:= XMLBase.KVdocs[0] else
  if XMLBase.KPTdocs.Count<>0 then  _node:= XMLBase.KPTdocs[0] else
  if XMLBase.KPdocs.Count<>0 then _node:= XMLBase.KPdocs[0];
  TRY
    Result:=_node.Name+','+_node.Certification_Doc.Date+','+_node.Certification_Doc.Number;
  EXCEPT
    Result:='������ ��� ��������� ��������.';
  END;
end;

function TDataBase.TransformXML(const aXMLs: TArray<String>; aTempDirectory, aDirectory: string): boolean;
var
  _i, _j: Integer;
  _base: IXMLXmlBase;
  _node: IXMLTXmlItem;
  _FileXML, _tempSt,_st2: string;
  _Files:TStringDynArray;
begin
  Result:=True;
  for _i := LOW(aXMLs) to High(aXMLs) do begin
    _FileXML:= aXMLs[_i];
    _tempSt:= TPath.ChangeExtension(_FileXML,'.inf');
    Common.doTransform(_FileXML,_tempSt);
    _base:= LoadXmlBase(_tempSt);
    if _base.KVdocs.Count<>0 then _node:= _base.KVdocs[0]
    else
      if _base.KPTdocs.Count<>0 then _node:= _base.KPTdocs[0]
      else
        if _base.KPdocs.Count<>0 then _node:= _base.KPdocs[0]
        else begin
          TDirectory.Delete(aTempDirectory,TRUE);
          EXIT(false);
        end;

    // �������� ��������� ����� ��� xml
    _st2:= aDirectory+tpath.DirectorySeparatorChar+ Common.getPath(_node);
    _tempSt:= common.getFileName(_Node);
    _node:= nil; _base:= nil;

    TDirectory.CreateDirectory(_st2);
    _Files:= TDirectory.GetFiles(aTempDirectory, TPath.GetFileNameWithoutExtension(_FileXML)+'.*');
    for _j:= 0 to high(_Files) do
      TFile.Copy(_Files[_j],_st2+ TPath.GetFileNameWithoutExtension(_tempSt)+tpath.GetExtension(_Files[_j]),true);
  end;
end;

procedure TDataBase.UpLoad(OnlyActual: boolean);
var
  _i:integer;
begin
 for _i := 0 to High(self.FFinedItems) do
  self.UpLoad(_i, OnlyActual);
 self.FKinds:=[];
end;

procedure TDataBase.UpLoad(I:integer; OnlyActual: boolean);
begin
   if TPath.GetExtension(self.FFinedItems[I])='' then
   self.UpLoadDirectory(self.FFinedItems[I], self.FDestDir, OnlyActual)
  else
    TFile.Copy(self.FFinedItems[I], self.FDestDir+'\'+TPath.GetFileName(self.FFinedItems[I]));
end;

procedure TDataBase.UpLoadDirectory(const aItem: string; aDestDir: string; OnlyActual: boolean);
var
  _i: integer;
  _SubDirs: TStringDynArray;
  _Files: TStringDynArray;
begin
  _SubDirs:= TDirectory.GetDirectories(aItem,'*.*',TSearchOption.soTopDirectoryOnly);
  for _i := 0 to High(_SubDirs) do
   self.UpLoadDirectory(_SubDirs[_i], aDestDir, OnlyActual);
  if dbiXML in self.FKinds then begin
   _Files:= TDirectory.GetFiles(aItem,rsMaskXML,TSearchOption.soTopDirectoryOnly);
   self.UpLoadFiles(_Files,aDestDir,OnlyActual);
  end;
  if dbiZIP in self.FKinds then begin
   _Files:= TDirectory.GetFiles(aItem,rsMaskZIP,TSearchOption.soTopDirectoryOnly);
   self.UpLoadFiles(_Files,aDestDir,OnlyActual);
  end;
end;

procedure TDataBase.UpLoadFiles(const aItems: TStringDynArray; aDestDir: string;
  OnlyActual: boolean);
var
 _i: integer;
begin
TRY
   for _i := 0 to High(aItems) do
    TFile.Copy(aItems[_i], self.FDestDir+tpath.DirectorySeparatorChar+TPath.GetFileName(aItems[_i]),true);
EXCEPT
  raise Exception.Create('������ ��������');
END;
end;

procedure TDataBase.UpLoadMask(I: Integer; OnlyActual: boolean);
begin
  if dbiXML in self.FKinds then begin
    if TFile.Exists(TPath.ChangeExtension(self.FFinedItems[I], '.xml')) then
      TFile.Copy(self.FFinedItems[I], self.FDestDir+tpath.DirectorySeparatorChar+TPath.GetFileName(TPath.ChangeExtension(self.FFinedItems[I], '.xml')), true);
  end;

  if dbiZIP in self.FKinds then begin
    if TFile.Exists(TPath.GetDirectoryName(self.FFinedItems[I])+TPath.DirectorySeparatorChar+TPath.GetFileNameWithoutExtension(self.FFinedItems[I])+'.zip') then
      TFile.Copy(TPath.GetDirectoryName(self.FFinedItems[I])+TPath.DirectorySeparatorChar+TPath.GetFileNameWithoutExtension(self.FFinedItems[I])+'.zip', self.FDestDir+tpath.DirectorySeparatorChar+TPath.GetFileName(TPath.ChangeExtension(self.FFinedItems[I], '.zip')), true);
  end;
end;

end.
