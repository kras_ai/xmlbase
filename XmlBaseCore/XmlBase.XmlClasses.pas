
{*****************************************************}
{                                                     }
{                  XML Data Binding                   }
{                                                     }
{         Generated on: 19.03.2014 13:24:56           }
{       Generated from: D:\Program\Test\XmlBase.xsd   }
{   Settings stored in: D:\Program\Test\XmlBase.xdb   }
{                                                     }
{*****************************************************}

unit XmlBase.XmlClasses;

interface

uses xmldom, XMLDoc, XMLIntf;

type

{ Forward Decls }

  IXMLXmlBase = interface;
  IXMLXmlBase_KVdocs = interface;
  IXMLTXmlItem = interface;
  IXMLTXmlItem_SubItems = interface;
  IXMLTXmlItem_SubItems_SubItem = interface;
  IXMLTXmlItem_Certification_Doc = interface;
  IXMLXmlBase_KPTdocs = interface;
  IXMLXmlBase_KPdocs = interface;
  IXMLXmlBase_EGRPdocs = interface;

{ IXMLXmlBase }

  IXMLXmlBase = interface(IXMLNode)
    ['{C99DA4BB-4D63-4FA3-A2D3-167C1798ED5E}']
    { Property Accessors }
    function Get_Version: UnicodeString;
    function Get_KVdocs: IXMLXmlBase_KVdocs;
    function Get_KPTdocs: IXMLXmlBase_KPTdocs;
    function Get_KPdocs: IXMLXmlBase_KPdocs;
    function Get_EGRPdocs: IXMLXmlBase_EGRPdocs;
    procedure Set_Version(Value: UnicodeString);
    { Methods & Properties }
    property Version: UnicodeString read Get_Version write Set_Version;
    property KVdocs: IXMLXmlBase_KVdocs read Get_KVdocs;
    property KPTdocs: IXMLXmlBase_KPTdocs read Get_KPTdocs;
    property KPdocs: IXMLXmlBase_KPdocs read Get_KPdocs;
    property EGRPdocs: IXMLXmlBase_EGRPdocs read Get_EGRPdocs;
  end;

{ IXMLXmlBase_KVdocs }

  IXMLXmlBase_KVdocs = interface(IXMLNodeCollection)
    ['{D5D05EE5-655C-49CD-BD18-E4785210C798}']
    { Property Accessors }
    function Get_KV(Index: Integer): IXMLTXmlItem;
    { Methods & Properties }
    function Add: IXMLTXmlItem;
    function Insert(const Index: Integer): IXMLTXmlItem;
    property KV[Index: Integer]: IXMLTXmlItem read Get_KV; default;
  end;

{ IXMLTXmlItem }

  IXMLTXmlItem = interface(IXMLNode)
    ['{B2A2C27F-34F7-4902-96B2-EDC23D49F02F}']
    { Property Accessors }
    function Get_CodeType: UnicodeString;
    function Get_Version: UnicodeString;
    function Get_Name: UnicodeString;
    function Get_Date: UnicodeString;
    function Get_SubItems: IXMLTXmlItem_SubItems;
    function Get_Certification_Doc: IXMLTXmlItem_Certification_Doc;
    procedure Set_CodeType(Value: UnicodeString);
    procedure Set_Version(Value: UnicodeString);
    procedure Set_Name(Value: UnicodeString);
    procedure Set_Date(Value: UnicodeString);
    { Methods & Properties }
    property CodeType: UnicodeString read Get_CodeType write Set_CodeType;
    property Version: UnicodeString read Get_Version write Set_Version;
    property Name: UnicodeString read Get_Name write Set_Name;
    property Date: UnicodeString read Get_Date write Set_Date;
    property SubItems: IXMLTXmlItem_SubItems read Get_SubItems;
    property Certification_Doc: IXMLTXmlItem_Certification_Doc read Get_Certification_Doc;
  end;

{ IXMLTXmlItem_SubItems }

  IXMLTXmlItem_SubItems = interface(IXMLNodeCollection)
    ['{BF389333-DD03-43C6-B3E9-A79955B65EB7}']
    { Property Accessors }
    function Get_SubItem(Index: Integer): IXMLTXmlItem_SubItems_SubItem;
    { Methods & Properties }
    function Add: IXMLTXmlItem_SubItems_SubItem;
    function Insert(const Index: Integer): IXMLTXmlItem_SubItems_SubItem;
    property SubItem[Index: Integer]: IXMLTXmlItem_SubItems_SubItem read Get_SubItem; default;
  end;

{ IXMLTXmlItem_SubItems_SubItem }

  IXMLTXmlItem_SubItems_SubItem = interface(IXMLNode)
    ['{3E137469-F4D9-4B61-8614-1DCA5B461999}']
    { Property Accessors }
    function Get_Name: UnicodeString;
    function Get_Code: UnicodeString;
    procedure Set_Name(Value: UnicodeString);
    procedure Set_Code(Value: UnicodeString);
    { Methods & Properties }
    property Name: UnicodeString read Get_Name write Set_Name;
    property Code: UnicodeString read Get_Code write Set_Code;
  end;

{ IXMLTXmlItem_Certification_Doc }

  IXMLTXmlItem_Certification_Doc = interface(IXMLNode)
    ['{DB8AEE81-222B-43CF-A9D8-8639BA7E3E56}']
    { Property Accessors }
    function Get_Organization: UnicodeString;
    function Get_Date: UnicodeString;
    function Get_Number: UnicodeString;
    function Get_Appointment: UnicodeString;
    function Get_FIO: UnicodeString;
    procedure Set_Organization(Value: UnicodeString);
    procedure Set_Date(Value: UnicodeString);
    procedure Set_Number(Value: UnicodeString);
    procedure Set_Appointment(Value: UnicodeString);
    procedure Set_FIO(Value: UnicodeString);
    { Methods & Properties }
    property Organization: UnicodeString read Get_Organization write Set_Organization;
    property Date: UnicodeString read Get_Date write Set_Date;
    property Number: UnicodeString read Get_Number write Set_Number;
    property Appointment: UnicodeString read Get_Appointment write Set_Appointment;
    property FIO: UnicodeString read Get_FIO write Set_FIO;
  end;

{ IXMLXmlBase_KPTdocs }

  IXMLXmlBase_KPTdocs = interface(IXMLNodeCollection)
    ['{1C2D5FEB-3A71-4D9E-8A6E-0997E7850B58}']
    { Property Accessors }
    function Get_KPT(Index: Integer): IXMLTXmlItem;
    { Methods & Properties }
    function Add: IXMLTXmlItem;
    function Insert(const Index: Integer): IXMLTXmlItem;
    property KPT[Index: Integer]: IXMLTXmlItem read Get_KPT; default;
  end;

{ IXMLXmlBase_KPdocs }

  IXMLXmlBase_KPdocs = interface(IXMLNodeCollection)
    ['{71D16DDF-6688-4D6C-A581-554DA2282D26}']
    { Property Accessors }
    function Get_KP(Index: Integer): IXMLTXmlItem;
    { Methods & Properties }
    function Add: IXMLTXmlItem;
    function Insert(const Index: Integer): IXMLTXmlItem;
    property KP[Index: Integer]: IXMLTXmlItem read Get_KP; default;
  end;

{ IXMLXmlBase_EGRPdocs }

  IXMLXmlBase_EGRPdocs = interface(IXMLNodeCollection)
    ['{949C06D7-FE23-4B74-95E9-F8585AC311CA}']
    { Property Accessors }
    function Get_EGRP(Index: Integer): IXMLTXmlItem;
    { Methods & Properties }
    function Add: IXMLTXmlItem;
    function Insert(const Index: Integer): IXMLTXmlItem;
    property EGRP[Index: Integer]: IXMLTXmlItem read Get_EGRP; default;
  end;

{ Forward Decls }

  TXMLXmlBase = class;
  TXMLXmlBase_KVdocs = class;
  TXMLTXmlItem = class;
  TXMLTXmlItem_SubItems = class;
  TXMLTXmlItem_SubItems_SubItem = class;
  TXMLTXmlItem_Certification_Doc = class;
  TXMLXmlBase_KPTdocs = class;
  TXMLXmlBase_KPdocs = class;
  TXMLXmlBase_EGRPdocs = class;

{ TXMLXmlBase }

  TXMLXmlBase = class(TXMLNode, IXMLXmlBase)
  protected
    { IXMLXmlBase }
    function Get_Version: UnicodeString;
    function Get_KVdocs: IXMLXmlBase_KVdocs;
    function Get_KPTdocs: IXMLXmlBase_KPTdocs;
    function Get_KPdocs: IXMLXmlBase_KPdocs;
    function Get_EGRPdocs: IXMLXmlBase_EGRPdocs;
    procedure Set_Version(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLXmlBase_KVdocs }

  TXMLXmlBase_KVdocs = class(TXMLNodeCollection, IXMLXmlBase_KVdocs)
  protected
    { IXMLXmlBase_KVdocs }
    function Get_KV(Index: Integer): IXMLTXmlItem;
    function Add: IXMLTXmlItem;
    function Insert(const Index: Integer): IXMLTXmlItem;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTXmlItem }

  TXMLTXmlItem = class(TXMLNode, IXMLTXmlItem)
  protected
    { IXMLTXmlItem }
    function Get_CodeType: UnicodeString;
    function Get_Version: UnicodeString;
    function Get_Name: UnicodeString;
    function Get_Date: UnicodeString;
    function Get_SubItems: IXMLTXmlItem_SubItems;
    function Get_Certification_Doc: IXMLTXmlItem_Certification_Doc;
    procedure Set_CodeType(Value: UnicodeString);
    procedure Set_Version(Value: UnicodeString);
    procedure Set_Name(Value: UnicodeString);
    procedure Set_Date(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTXmlItem_SubItems }

  TXMLTXmlItem_SubItems = class(TXMLNodeCollection, IXMLTXmlItem_SubItems)
  protected
    { IXMLTXmlItem_SubItems }
    function Get_SubItem(Index: Integer): IXMLTXmlItem_SubItems_SubItem;
    function Add: IXMLTXmlItem_SubItems_SubItem;
    function Insert(const Index: Integer): IXMLTXmlItem_SubItems_SubItem;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTXmlItem_SubItems_SubItem }

  TXMLTXmlItem_SubItems_SubItem = class(TXMLNode, IXMLTXmlItem_SubItems_SubItem)
  protected
    { IXMLTXmlItem_SubItems_SubItem }
    function Get_Name: UnicodeString;
    function Get_Code: UnicodeString;
    procedure Set_Name(Value: UnicodeString);
    procedure Set_Code(Value: UnicodeString);
  end;

{ TXMLTXmlItem_Certification_Doc }

  TXMLTXmlItem_Certification_Doc = class(TXMLNode, IXMLTXmlItem_Certification_Doc)
  protected
    { IXMLTXmlItem_Certification_Doc }
    function Get_Organization: UnicodeString;
    function Get_Date: UnicodeString;
    function Get_Number: UnicodeString;
    function Get_Appointment: UnicodeString;
    function Get_FIO: UnicodeString;
    procedure Set_Organization(Value: UnicodeString);
    procedure Set_Date(Value: UnicodeString);
    procedure Set_Number(Value: UnicodeString);
    procedure Set_Appointment(Value: UnicodeString);
    procedure Set_FIO(Value: UnicodeString);
  end;

{ TXMLXmlBase_KPTdocs }

  TXMLXmlBase_KPTdocs = class(TXMLNodeCollection, IXMLXmlBase_KPTdocs)
  protected
    { IXMLXmlBase_KPTdocs }
    function Get_KPT(Index: Integer): IXMLTXmlItem;
    function Add: IXMLTXmlItem;
    function Insert(const Index: Integer): IXMLTXmlItem;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLXmlBase_KPdocs }

  TXMLXmlBase_KPdocs = class(TXMLNodeCollection, IXMLXmlBase_KPdocs)
  protected
    { IXMLXmlBase_KPdocs }
    function Get_KP(Index: Integer): IXMLTXmlItem;
    function Add: IXMLTXmlItem;
    function Insert(const Index: Integer): IXMLTXmlItem;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLXmlBase_EGRPdocs }

  TXMLXmlBase_EGRPdocs = class(TXMLNodeCollection, IXMLXmlBase_EGRPdocs)
  protected
    { IXMLXmlBase_EGRPdocs }
    function Get_EGRP(Index: Integer): IXMLTXmlItem;
    function Add: IXMLTXmlItem;
    function Insert(const Index: Integer): IXMLTXmlItem;
  public
    procedure AfterConstruction; override;
  end;

{ Global Functions }

function GetXmlBase(Doc: IXMLDocument): IXMLXmlBase;
function LoadXmlBase(const FileName: string): IXMLXmlBase;
function NewXmlBase: IXMLXmlBase;

const
  TargetNamespace = '';

implementation

{ Global Functions }

function GetXmlBase(Doc: IXMLDocument): IXMLXmlBase;
begin
  Result := Doc.GetDocBinding('XmlBase', TXMLXmlBase, TargetNamespace) as IXMLXmlBase;
end;

function LoadXmlBase(const FileName: string): IXMLXmlBase;
begin
  Result := LoadXMLDocument(FileName).GetDocBinding('XmlBase', TXMLXmlBase, TargetNamespace) as IXMLXmlBase;
end;

function NewXmlBase: IXMLXmlBase;
begin
  Result := NewXMLDocument.GetDocBinding('XmlBase', TXMLXmlBase, TargetNamespace) as IXMLXmlBase;
end;

{ TXMLXmlBase }

procedure TXMLXmlBase.AfterConstruction;
begin
  RegisterChildNode('KVdocs', TXMLXmlBase_KVdocs);
  RegisterChildNode('KPTdocs', TXMLXmlBase_KPTdocs);
  RegisterChildNode('KPdocs', TXMLXmlBase_KPdocs);
  RegisterChildNode('EGRPdocs', TXMLXmlBase_EGRPdocs);
  inherited;
end;

function TXMLXmlBase.Get_Version: UnicodeString;
begin
  Result := AttributeNodes['Version'].Text;
end;

procedure TXMLXmlBase.Set_Version(Value: UnicodeString);
begin
  SetAttribute('Version', Value);
end;

function TXMLXmlBase.Get_KVdocs: IXMLXmlBase_KVdocs;
begin
  Result := ChildNodes['KVdocs'] as IXMLXmlBase_KVdocs;
end;

function TXMLXmlBase.Get_KPTdocs: IXMLXmlBase_KPTdocs;
begin
  Result := ChildNodes['KPTdocs'] as IXMLXmlBase_KPTdocs;
end;

function TXMLXmlBase.Get_KPdocs: IXMLXmlBase_KPdocs;
begin
  Result := ChildNodes['KPdocs'] as IXMLXmlBase_KPdocs;
end;

function TXMLXmlBase.Get_EGRPdocs: IXMLXmlBase_EGRPdocs;
begin
  Result := ChildNodes['EGRPdocs'] as IXMLXmlBase_EGRPdocs;
end;

{ TXMLXmlBase_KVdocs }

procedure TXMLXmlBase_KVdocs.AfterConstruction;
begin
  RegisterChildNode('KV', TXMLTXmlItem);
  ItemTag := 'KV';
  ItemInterface := IXMLTXmlItem;
  inherited;
end;

function TXMLXmlBase_KVdocs.Get_KV(Index: Integer): IXMLTXmlItem;
begin
  Result := List[Index] as IXMLTXmlItem;
end;

function TXMLXmlBase_KVdocs.Add: IXMLTXmlItem;
begin
  Result := AddItem(-1) as IXMLTXmlItem;
end;

function TXMLXmlBase_KVdocs.Insert(const Index: Integer): IXMLTXmlItem;
begin
  Result := AddItem(Index) as IXMLTXmlItem;
end;

{ TXMLTXmlItem }

procedure TXMLTXmlItem.AfterConstruction;
begin
  RegisterChildNode('SubItems', TXMLTXmlItem_SubItems);
  RegisterChildNode('Certification_Doc', TXMLTXmlItem_Certification_Doc);
  inherited;
end;

function TXMLTXmlItem.Get_CodeType: UnicodeString;
begin
  Result := AttributeNodes['CodeType'].Text;
end;

procedure TXMLTXmlItem.Set_CodeType(Value: UnicodeString);
begin
  SetAttribute('CodeType', Value);
end;

function TXMLTXmlItem.Get_Version: UnicodeString;
begin
  Result := AttributeNodes['Version'].Text;
end;

procedure TXMLTXmlItem.Set_Version(Value: UnicodeString);
begin
  SetAttribute('Version', Value);
end;

function TXMLTXmlItem.Get_Name: UnicodeString;
begin
  Result := AttributeNodes['Name'].Text;
end;

procedure TXMLTXmlItem.Set_Name(Value: UnicodeString);
begin
  SetAttribute('Name', Value);
end;

function TXMLTXmlItem.Get_Date: UnicodeString;
begin
  Result := AttributeNodes['Date'].Text;
end;

procedure TXMLTXmlItem.Set_Date(Value: UnicodeString);
begin
  SetAttribute('Date', Value);
end;

function TXMLTXmlItem.Get_SubItems: IXMLTXmlItem_SubItems;
begin
  Result := ChildNodes['SubItems'] as IXMLTXmlItem_SubItems;
end;

function TXMLTXmlItem.Get_Certification_Doc: IXMLTXmlItem_Certification_Doc;
begin
  Result := ChildNodes['Certification_Doc'] as IXMLTXmlItem_Certification_Doc;
end;

{ TXMLTXmlItem_SubItems }

procedure TXMLTXmlItem_SubItems.AfterConstruction;
begin
  RegisterChildNode('SubItem', TXMLTXmlItem_SubItems_SubItem);
  ItemTag := 'SubItem';
  ItemInterface := IXMLTXmlItem_SubItems_SubItem;
  inherited;
end;

function TXMLTXmlItem_SubItems.Get_SubItem(Index: Integer): IXMLTXmlItem_SubItems_SubItem;
begin
  Result := List[Index] as IXMLTXmlItem_SubItems_SubItem;
end;

function TXMLTXmlItem_SubItems.Add: IXMLTXmlItem_SubItems_SubItem;
begin
  Result := AddItem(-1) as IXMLTXmlItem_SubItems_SubItem;
end;

function TXMLTXmlItem_SubItems.Insert(const Index: Integer): IXMLTXmlItem_SubItems_SubItem;
begin
  Result := AddItem(Index) as IXMLTXmlItem_SubItems_SubItem;
end;

{ TXMLTXmlItem_SubItems_SubItem }

function TXMLTXmlItem_SubItems_SubItem.Get_Name: UnicodeString;
begin
  Result := AttributeNodes['Name'].Text;
end;

procedure TXMLTXmlItem_SubItems_SubItem.Set_Name(Value: UnicodeString);
begin
  SetAttribute('Name', Value);
end;

function TXMLTXmlItem_SubItems_SubItem.Get_Code: UnicodeString;
begin
  Result := AttributeNodes['Code'].Text;
end;

procedure TXMLTXmlItem_SubItems_SubItem.Set_Code(Value: UnicodeString);
begin
  SetAttribute('Code', Value);
end;

{ TXMLTXmlItem_Certification_Doc }

function TXMLTXmlItem_Certification_Doc.Get_Organization: UnicodeString;
begin
  Result := ChildNodes['Organization'].Text;
end;

procedure TXMLTXmlItem_Certification_Doc.Set_Organization(Value: UnicodeString);
begin
  ChildNodes['Organization'].NodeValue := Value;
end;

function TXMLTXmlItem_Certification_Doc.Get_Date: UnicodeString;
begin
  Result := ChildNodes['Date'].Text;
end;

procedure TXMLTXmlItem_Certification_Doc.Set_Date(Value: UnicodeString);
begin
  ChildNodes['Date'].NodeValue := Value;
end;

function TXMLTXmlItem_Certification_Doc.Get_Number: UnicodeString;
begin
  Result := ChildNodes['Number'].Text;
end;

procedure TXMLTXmlItem_Certification_Doc.Set_Number(Value: UnicodeString);
begin
  ChildNodes['Number'].NodeValue := Value;
end;

function TXMLTXmlItem_Certification_Doc.Get_Appointment: UnicodeString;
begin
  Result := ChildNodes['Appointment'].Text;
end;

procedure TXMLTXmlItem_Certification_Doc.Set_Appointment(Value: UnicodeString);
begin
  ChildNodes['Appointment'].NodeValue := Value;
end;

function TXMLTXmlItem_Certification_Doc.Get_FIO: UnicodeString;
begin
  Result := ChildNodes['FIO'].Text;
end;

procedure TXMLTXmlItem_Certification_Doc.Set_FIO(Value: UnicodeString);
begin
  ChildNodes['FIO'].NodeValue := Value;
end;

{ TXMLXmlBase_KPTdocs }

procedure TXMLXmlBase_KPTdocs.AfterConstruction;
begin
  RegisterChildNode('KPT', TXMLTXmlItem);
  ItemTag := 'KPT';
  ItemInterface := IXMLTXmlItem;
  inherited;
end;

function TXMLXmlBase_KPTdocs.Get_KPT(Index: Integer): IXMLTXmlItem;
begin
  Result := List[Index] as IXMLTXmlItem;
end;

function TXMLXmlBase_KPTdocs.Add: IXMLTXmlItem;
begin
  Result := AddItem(-1) as IXMLTXmlItem;
end;

function TXMLXmlBase_KPTdocs.Insert(const Index: Integer): IXMLTXmlItem;
begin
  Result := AddItem(Index) as IXMLTXmlItem;
end;

{ TXMLXmlBase_KPdocs }

procedure TXMLXmlBase_KPdocs.AfterConstruction;
begin
  RegisterChildNode('KP', TXMLTXmlItem);
  ItemTag := 'KP';
  ItemInterface := IXMLTXmlItem;
  inherited;
end;

function TXMLXmlBase_KPdocs.Get_KP(Index: Integer): IXMLTXmlItem;
begin
  Result := List[Index] as IXMLTXmlItem;
end;

function TXMLXmlBase_KPdocs.Add: IXMLTXmlItem;
begin
  Result := AddItem(-1) as IXMLTXmlItem;
end;

function TXMLXmlBase_KPdocs.Insert(const Index: Integer): IXMLTXmlItem;
begin
  Result := AddItem(Index) as IXMLTXmlItem;
end;

{ TXMLXmlBase_EGRPdocs }

procedure TXMLXmlBase_EGRPdocs.AfterConstruction;
begin
  RegisterChildNode('EGRP', TXMLTXmlItem);
  ItemTag := 'EGRP';
  ItemInterface := IXMLTXmlItem;
  inherited;
end;

function TXMLXmlBase_EGRPdocs.Get_EGRP(Index: Integer): IXMLTXmlItem;
begin
  Result := List[Index] as IXMLTXmlItem;
end;

function TXMLXmlBase_EGRPdocs.Add: IXMLTXmlItem;
begin
  Result := AddItem(-1) as IXMLTXmlItem;
end;

function TXMLXmlBase_EGRPdocs.Insert(const Index: Integer): IXMLTXmlItem;
begin
  Result := AddItem(Index) as IXMLTXmlItem;
end;

end.