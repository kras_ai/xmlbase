unit XmlBase.DataBase;

interface
 uses
  System.Types,
  System.IOUtils,
  System.SysUtils,
  System.Classes,
  System.Regularexpressions,
  Vcl.StdCtrls,
  sevenzip,
  MsStringParser;


type
  DataBase = record
   public type
    TItemKind = (dbiXML,dbiZIP);
    TItemKinds = set of TItemKind;
   public class var
    Directory: string;
    TempDirectory: string;
    Format:TFormatSettings;
   private
    class procedure UpLoadFiles(const aItems: TStringDynArray; aDestDir: string; OnlyActual: boolean = true); static;
    class procedure UpLoadDirectory(const aItem: string; aKinds: TItemKinds; aDestDir: string; OnlyActual: boolean = true); static;
   public
    class function LoadZip(const aFileName: string; log: TStringBuilder = nil): boolean; static;
    class function LoadZipEx(const aFileName,aTempDirectory,aDirectory: string; log: TStringBuilder = nil): boolean; static;
    class function FindByCadNum(const aCadNum: string): TStringDynArray; static;
    class function FindbyNumList(CadNums:String):TStringDynArray; static;
    class procedure UpLoad(const aItems: TStringDynArray; aKinds: TItemKinds; aDestDir: string; OnlyActual: boolean = true);overload; static;
    class procedure UpLoad(const aItem: string; aKinds: TItemKinds; aDestDir: string; OnlyActual: boolean = true);overload; static;

    // version 0.2
    class function FindByMask(const aMask:string): TStringDynArray; static;
    class function FindByMaskList(const aMaskList:string; var MemoFind:TMemo): TStringDynArray; static;
    class procedure UpLoadMask(const aItem:string; aKinds: TItemKinds; aDestDir: string; OnlyActual: boolean = true); static;
  end;


 // ��������� ����� �� ������
//procedure ExtractFiles(FileName, TempDir, DestDir:string);


implementation

uses XmlBase.Common, XmlBase.XmlClasses;
 resourcestring
   rsMaskXML = '*.xml';
   rsMaskZIP = '*.zip';

//��������� ����� �� ������
procedure ExtractFiles(FileName, TempDir, DestDir:string);
var
  _Svz: I7zInArchive;
  _PathName:string;
  _FileXML: string;
  _Files:TStringDynArray;
  _i,_j:integer;
  _base: IXMLXmlBase;
  _node: IXMLTXmlItem;
  _tempSt: string;
  _mStream: TMemoryStream;
begin
 _Svz:= CreateInArchive(CLSID_CFormatZip);
 _Svz.OpenFile(FileName);

 //��������� ���� �� � ��������� ������ xml ���� (� �����)
 //���� ������� ������� �������� ������ ���
 _mStream:= nil;  _FileXML:= '';
 for _i := 0 to _svz.NumberOfItems-1 do
  if '.xml'= AnsiLowerCase(tpath.GetExtension(_svz.ItemPath[_i])) then begin
   _mStream:= TMemoryStream.Create;
   _Svz.ExtractItem(_i,_mStream,false);
   TDirectory.CreateDirectory(TempDir);
   _FileXML:= TempDir+tpath.DirectorySeparatorChar+TPath.GetFileName(_svz.ItemPath[_i]);
   _mStream.SaveToFile(_FileXML);
   BREAK;
  end;
 TRY
 //���� � ������ ������ ���� ������� xml
 if _FileXML<>'' then begin
  //��� �������� ���� xml
  FreeAndNil(_mStream);

  _tempSt:= TPath.ChangeExtension(_FileXML,'.inf');
  Common.doTransform(_FileXML,_tempSt);
//  if not Common.isValid(_tempSt) then raise

  _base:= LoadXmlBase(_tempSt);
  if _base.KVdocs.Count<>0 then _node:= _base.KVdocs[0] else
  if _base.KPTdocs.Count<>0 then _node:= _base.KPTdocs[0] else
  if _base.KPdocs.Count<>0 then _node:= _base.KPdocs[0];// else

  DestDir:= DestDir+tpath.DirectorySeparatorChar+ Common.getPath(_node);
  _tempSt:= common.getFileName(_Node);
  _node:= nil; _base:= nil;

  TDirectory.CreateDirectory(DestDir);
  _Files:= TDirectory.GetFiles(TempDir,'*.*');
  for _j:= 0 to high(_Files) do
   TFile.Copy(_Files[_j],DestDir+ TPath.GetFileNameWithoutExtension(_tempSt)+tpath.GetExtension(_Files[_j]),true);
  TFile.Copy(FileName,DestDir+TPath.GetFileNameWithoutExtension(_tempSt)+'.zip',true);
 end else begin //���� � ������ �� ���� ������� xml �����
  _Svz.ExtractTo(TempDir);
  //���� � ������������� ������ ������ ������
  _Files:= TDirectory.GetFiles(TempDir,rsMaskZIP,TSearchOption.soAllDirectories);
  for _i := 0 to high (_Files) do begin
   _PathName:= TPath.ChangeExtension(_Files[_i],'');
   SetLength(_PathName,length(_PathName)-1);
   ExtractFiles(_Files[_i], _PathName, DestDir);
  end;
 end;
  TDirectory.Delete(TempDir,TRUE);

 EXCEPT
  TDirectory.Delete(TempDir,TRUE);
 END;
end;

{ DataBase }

class function DataBase.LoadZipEx(const aFileName, aTempDirectory, aDirectory: string; log: TStringBuilder): boolean;
var
  _Svz: I7zInArchive;
  _PathName:string;
  _FileXML: string;
  _Files:TStringDynArray;
  _i,_j:integer;
  _base: IXMLXmlBase;
  _node: IXMLTXmlItem;
  _tempSt,_st2: string;
  _mStream: TMemoryStream;
begin
 _Svz:= CreateInArchive(CLSID_CFormatZip);
 _Svz.OpenFile(aFileName);

 //��������� ���� �� � ��������� ������ xml ���� (� �����)
 //���� ������� ������� �������� ������ ���
 _mStream:= nil;  _FileXML:= '';
 for _i := 0 to _svz.NumberOfItems-1 do
  if '.xml'= AnsiLowerCase(tpath.GetExtension(_svz.ItemPath[_i])) then begin
   _mStream:= TMemoryStream.Create;
   _Svz.ExtractItem(_i,_mStream,false);
   TDirectory.CreateDirectory(aTempDirectory);
   _FileXML:= aTempDirectory+tpath.DirectorySeparatorChar+TPath.GetFileName(_svz.ItemPath[_i]);
   _mStream.SaveToFile(_FileXML);
   FreeAndNil(_mStream);
   BREAK;
  end;
 TRY
 //���� � ������ ������ ���� ������� xml
 if _FileXML<>'' then begin
  _tempSt:= TPath.ChangeExtension(_FileXML,'.inf');
  Common.doTransform(_FileXML,_tempSt);
  //  if not Common.isValid(_tempSt) then raise

  _base:= LoadXmlBase(_tempSt);
  if _base.KVdocs.Count<>0 then _node:= _base.KVdocs[0] else
  if _base.KPTdocs.Count<>0 then _node:= _base.KPTdocs[0] else
  if _base.KPdocs.Count<>0 then _node:= _base.KPdocs[0] else begin
     TDirectory.Delete(aTempDirectory,TRUE);
    EXIT;
  end;

  _st2:= aDirectory+tpath.DirectorySeparatorChar+ Common.getPath(_node);
  _tempSt:= common.getFileName(_Node);
  _node:= nil; _base:= nil;

  TDirectory.CreateDirectory(_st2);
  _Files:= TDirectory.GetFiles(aTempDirectory,'*.*');
  for _j:= 0 to high(_Files) do
   TFile.Copy(_Files[_j],_st2+ TPath.GetFileNameWithoutExtension(_tempSt)+tpath.GetExtension(_Files[_j]),true);
  TFile.Copy(aFileName,_st2+TPath.GetFileNameWithoutExtension(_tempSt)+'.zip',true);
 end else begin //���� � ������ �� ���� ������� xml �����
  _Svz.ExtractTo(aTempDirectory);
  //���� � ������������� ������ ������ ������
  _Files:= TDirectory.GetFiles(aTempDirectory,rsMaskZIP,TSearchOption.soAllDirectories);
  for _i := 0 to high (_Files) do begin
   _PathName:= TPath.ChangeExtension(_Files[_i],'');
   SetLength(_PathName,length(_PathName)-1);
   DataBase.LoadZipEx(_Files[_i],_PathName,aDirectory,log);
  end;
 end;
  TDirectory.Delete(aTempDirectory,TRUE);
 EXCEPT
  //TDirectory.Delete(aTempDirectory,TRUE);
 END;
end;


class procedure DataBase.UpLoad(const aItems: TStringDynArray; aKinds: TItemKinds; aDestDir: string; OnlyActual: boolean);
var
  _i:integer;
begin
 for _i := 0 to High(aItems) do
  DataBase.UpLoad(aItems[_i],aKinds,aDestDir,OnlyActual);
end;

class procedure DataBase.UpLoad(const aItem: string; aKinds: TItemKinds; aDestDir: string; OnlyActual: boolean);
begin
   if TPath.GetExtension(aItem)='' then
   DataBase.UpLoadDirectory(aItem,aKinds,aDestDir,OnlyActual)
  else
    TFile.Copy(aItem, aDestDir+'\'+TPath.GetFileName(aItem));
end;

class procedure DataBase.UpLoadDirectory(const aItem: String;
  aKinds: TItemKinds; aDestDir: string; OnlyActual: boolean);
var
  _i: integer;
  _SubDirs: TStringDynArray;
  _Files: TStringDynArray;
begin
  _SubDirs:= TDirectory.GetDirectories(aItem,'*.*',TSearchOption.soTopDirectoryOnly);
  for _i := 0 to High(_SubDirs) do
   DataBase.UpLoadDirectory(_SubDirs[_i],aKinds, aDestDir, OnlyActual);
  if dbiXML in aKinds then begin
   _Files:= TDirectory.GetFiles(aItem,rsMaskXML,TSearchOption.soTopDirectoryOnly);
   DataBase.UpLoadFiles(_Files,aDestDir,OnlyActual);
  end;
  if dbiZIP in aKinds then begin
   _Files:= TDirectory.GetFiles(aItem,rsMaskZIP,TSearchOption.soTopDirectoryOnly);
   DataBase.UpLoadFiles(_Files,aDestDir,OnlyActual);
  end;
end;

class procedure DataBase.UpLoadFiles(const aItems: TStringDynArray;
  aDestDir: string; OnlyActual: boolean);
var
 _i: integer;
begin
TRY
   for _i := 0 to High(aItems) do
    TFile.Copy(aItems[_i], aDestDir+tpath.DirectorySeparatorChar+TPath.GetFileName(aItems[_i]),true);
EXCEPT
  raise Exception.Create(aItems[_i]);
END;
end;

class procedure DataBase.UpLoadMask(const aItem: string; aKinds: TItemKinds; aDestDir: string; OnlyActual: boolean);
begin
  if dbiXML in aKinds then begin
    if TFile.Exists(TPath.ChangeExtension(aItem, '.xml')) then
      TFile.Copy(aItem, aDestDir+tpath.DirectorySeparatorChar+TPath.GetFileName(TPath.ChangeExtension(aItem, '.xml')), true);
  end;

  if dbiZIP in aKinds then begin
    if TFile.Exists(TPath.GetDirectoryName(aItem)+TPath.DirectorySeparatorChar+TPath.GetFileNameWithoutExtension(aItem)+'.zip') then
      TFile.Copy(TPath.GetDirectoryName(aItem)+TPath.DirectorySeparatorChar+TPath.GetFileNameWithoutExtension(aItem)+'.zip', aDestDir+tpath.DirectorySeparatorChar+TPath.GetFileName(TPath.ChangeExtension(aItem, '.zip')), true);
  end;
end;

class function DataBase.FindByCadNum(const aCadNum: string): TStringDynArray;
var
  _stb:TstringBuilder;
  _st: string;
  _i,_j:integer;
  _str:TMSStringParser;
  _sda:  TStringDynArray;
begin
  _str:=TMSStringParser.Create(' ;:');
  _stb:=TStringBuilder.Create;
  _str.Parse(aCadNum);
  for _i := 0 to _str.GetCountToken-1 do begin
   for _j := 0 to _i do begin
    _stb.Append(_str.GetToken(_j));
    if _j<>_i then
      _stb.Append(' ');
   end;
   _stb.Append(TPath.DirectorySeparatorChar);
  end;
  _str.Free;
  _st:= DataBase.Directory+TPath.DirectorySeparatorChar+_stb.ToString;
  _stb.Free;

  if TDirectory.Exists(_st) then begin
    Result:=TDirectory.GetDirectories(_st, '*.*', TSearchOption.soTopDirectoryOnly);
    _sda:=TDirectory.GetFiles(_st, '*.*', TSearchOption.soTopDirectoryOnly);
    _i:= Length(Result);
    _j:= Length(_sda);
    SetLength(Result,_i+_j);
    for _j := 0 to High(_sda) do
     Result[_i+_j]:= _sda[_j];
    _sda:= nil;
  end;
end;

class function DataBase.FindbyNumList(CadNums: String): TStringDynArray;
var
  _strP:TMSStringParser;
  _k,_i,_j:integer;
  _Find:TStringDynArray;
  _st:string;
begin
  _strP:=TMSStringParser.Create(','+sLineBreak);
  _strP.Parse(CadNums);
  if _strP.GetCountToken<>0 then begin
   for _k := 0 to _strP.GetCountToken-1 do
   begin
    _Find:=DataBase.FindByCadNum(_strP.GetToken(_k));
    _i:= Length(Result);
    _j:= Length(_Find);
    SetLength(Result,_i+_j);
    for _j := 0 to High(_Find) do
      Result[_i+_j]:=_Find[_j];
    _Find:= nil;
   end;
  end else begin
    _st:= DataBase.Directory;
    if TDirectory.Exists(_st) then
      Result:=TDirectory.GetDirectories(_st, '*.*', TSearchOption.soTopDirectoryOnly);
  end;
end;


class function DataBase.LoadZip(const aFileName: string; log: TStringBuilder): boolean;
begin
 Result:= DataBase.LoadZipEx(aFileName,DataBase.TempDirectory,DataBase.Directory);
end;


class function DataBase.FindByMask(const aMask: string): TStringDynArray;
var
  _stb:TstringBuilder;
  _st: string;
  _i,_j:integer;
  _str:TMSStringParser;
  _sda:  TStringDynArray;
  _mask:string;
begin
  _str:=TMSStringParser.Create(' ;:');
  _stb:=TStringBuilder.Create;
  _str.Parse(aMask);
  if _str.GetCountToken=1 then
  begin
    _st:=_str.GetToken(0);
    _i:=Length(_str.GetToken(0));
    if _str.GetToken(0)[Length(_str.GetToken(0))]='*' then
     begin
      Delete(_st,_i,1);
     end;
     _stb.Append(Copy(_st,1,_i)).Append(TPath.DirectorySeparatorChar);
     _mask:=_str.GetToken(0);
  end
    else
  begin
    for _i := 0 to _str.GetCountToken-1 do begin
      for _j := 0 to _i do begin
        if POS('*',_str.GetToken(_i))=0 then
          _stb.Append(_str.GetToken(_j));
        if (_j<>_i) then
          _stb.Append(' ');
      end;
    if POS('*',_str.GetToken(_i))<>0 then
      BREAK
    else
      _stb.Append(TPath.DirectorySeparatorChar);
    end;

    for _i := 0 to _str.GetCountToken-1 do
      _mask:=_mask+' '+_str.GetToken(_i);
  end;


  _st:= DataBase.Directory+TPath.DirectorySeparatorChar+_stb.ToString;
  _str.Free;
  _stb.Free;

  if TDirectory.Exists(_st) then begin
    _sda:=TDirectory.GetFiles(TRIM(_st), TRIM(_mask)+'*.xml', TSearchOption.soAllDirectories);
  _j:= Length(_sda);
  SetLength(Result,_j);
  for _j := 0 to High(_sda) do
    Result[_j]:= _sda[_j];
  _sda:= nil;
  end;
end;

class function DataBase.FindByMaskList(const aMaskList: string; var MemoFind:TMemo): TStringDynArray;
var
  _strP:TMSStringParser;
  _k,_i,_j:integer;
  _Find:TStringDynArray;
  _st:string;
begin
  _strP:=TMSStringParser.Create(','+sLineBreak);
  _strP.Parse(aMaskList);
  if _strP.GetCountToken<>0 then begin
   for _k := 0 to _strP.GetCountToken-1 do
   begin
    _Find:=DataBase.FindByMask(_strP.GetToken(_k));
    if Length(_Find)<>0 then
    begin
      _i:= Length(Result);
      _j:= Length(_Find);
      SetLength(Result,_i+_j);
      for _j := 0 to High(_Find) do
        Result[_i+_j]:=_Find[_j];
      _Find:= nil;
    end
      else
      MemoFind.Lines.Add('����� �� ������� "'+_strP.GetToken(_k)+'" �� ��� �����������.')
   end;
  end else begin
    _st:= DataBase.Directory;
    if TDirectory.Exists(_st) then
      Result:=TDirectory.GetDirectories(_st, '*.*', TSearchOption.soTopDirectoryOnly);
  end;
end;


initialization
 DataBase.Format:= TFormatSettings.Create;

end.
