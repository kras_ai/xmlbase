unit Mgis.UI.SelectDirectory;

interface
uses
 System.sysutils,
 System.IOUtils,
 VCL.Dialogs,
 VCL.FileCtrl;

 function ExecuteDlgSelDirectory(var Path: string): boolean;

implementation
resourcestring
 rsSelDir = '����� �����';

function ExecuteDlgSelDirectory(var Path: string): boolean;
var
 _fo: TFileOpenDialog;
begin
  Result:= false;
  if (Win32MajorVersion >= 6) and UseLatestCommonDialogs then begin
   _fo:= TFileOpenDialog.Create(nil);
   TRY
   _fo.Options:= [TFileDialogOption.fdoPickFolders];
   _fo.OkButtonLabel:= rsSelDir;
   Result:= _fo.Execute;
   if Result then Path:= _fo.FileName;
   FINALLY
    _fo.Free;
   END;
  end else
   Result:= SelectDirectory(rsSelDir, rsSelDir, Path, [sdNewFolder, sdNewUI], nil);
  if Result then Path:= Path + Tpath.DirectorySeparatorChar;
end;





end.
