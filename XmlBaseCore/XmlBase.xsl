<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	
	xmlns:kpt10="urn://x-artefacts-rosreestr-ru/outgoing/kpt/10.0.1"
	xmlns:kpt09="urn://x-artefacts-rosreestr-ru/outgoing/kpt/9.0.3"
	
	xmlns:kvzu07="urn://x-artefacts-rosreestr-ru/outgoing/kvzu/7.0.1" 
	xmlns:kvzu06="urn://x-artefacts-rosreestr-ru/outgoing/kvzu/6.0.9" 
	
	xmlns:kpzu06="urn://x-artefacts-rosreestr-ru/outgoing/kpzu/6.0.1"
	xmlns:kpzu05='urn://x-artefacts-rosreestr-ru/outgoing/kpzu/5.0.8'
	
	xmlns:kpoks03="urn://x-artefacts-rosreestr-ru/outgoing/kpoks/3.0.7"
	xmlns:Cer1="urn://x-artefacts-rosreestr-ru/commons/complex-types/certification-doc/1.0"
	xmlns:ns7="urn://x-artefacts-smev-gov-ru/supplementary/commons/1.0.1">
	<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
	<xsl:strip-space elements="*"/>
	<!-- =========================================================================================== -->
	<xsl:template match="/*">
		<xsl:element name="XmlBase">
			<xsl:attribute name="Version"><xsl:text>0.1</xsl:text></xsl:attribute>
					
			<xsl:if test="local-name(self::node())='Region_Cadastr_Vidimus_KV'">
				<xsl:call-template name="Region_Cadastr_Vidimus_KV"/>
			</xsl:if>
			<xsl:if test="local-name(self::node())='Region_Cadastr'">
				<xsl:call-template name="Region_Cadastr"/>
			</xsl:if>
			<xsl:if test="local-name(self::node())='Region_Cadastr_Vidimus_KP'">
				<xsl:call-template name="Region_Cadastr_Vidimus_KP"/>
			</xsl:if>
			<xsl:if test="local-name(self::node())='Extract'">
				<xsl:call-template name="Extract"/>
			</xsl:if>
			
			<xsl:variable name="_vNS" select="namespace-uri(self::node())"/>
			<xsl:choose>
				<xsl:when test="$_vNS='urn://x-artefacts-rosreestr-ru/outgoing/kvzu/7.0.1'">
					<xsl:call-template name="KVZU07"/>
				</xsl:when>	
				<xsl:when test="$_vNS='urn://x-artefacts-rosreestr-ru/outgoing/kvzu/6.0.9'">
					<xsl:call-template name="KVZU06"/>
				</xsl:when>
				<xsl:when test="$_vNS='urn://x-artefacts-rosreestr-ru/outgoing/kpt/10.0.1'">
					<xsl:call-template name="KPT10"/>
				</xsl:when>	
				<xsl:when test="$_vNS='urn://x-artefacts-rosreestr-ru/outgoing/kpt/9.0.3'">
					<xsl:call-template name="KPT09"/>
				</xsl:when>
				
				<xsl:when test="$_vNS='urn://x-artefacts-rosreestr-ru/outgoing/kpzu/6.0.1'">
					<xsl:call-template name="KPZU06"/>
				</xsl:when>
				<xsl:when test="$_vNS='urn://x-artefacts-rosreestr-ru/outgoing/kpzu/5.0.8'">
					<xsl:call-template name="KPZU05"/>
				</xsl:when>	
				
				<xsl:when test="$_vNS='urn://x-artefacts-rosreestr-ru/outgoing/kpoks/3.0.7'">
					<xsl:call-template name="KPOKS03"/>
				</xsl:when>
			</xsl:choose>	
		</xsl:element>
		<!--xsl:value-of select="concat(//Certification_Doc/Date,' ')"/-->
		<!--xsl:apply-templates select="//Parcels"/-->
	</xsl:template>
	<!--Получание кадастрового номера-->
	<xsl:template name="GetCadNum_ByParcel">
		<xsl:variable name="vCadastralNumber" select="@CadastralNumber"/>
		<xsl:variable name="vKvartal" select="parent::node()/parent::node()/@CadastralNumber"/>
		<xsl:choose>
			<xsl:when test="string-length($vCadastralNumber)>string-length($vKvartal)">
				<xsl:value-of select="$vCadastralNumber"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="concat($vKvartal,$vCadastralNumber)"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!-- =========================================================================================== -->
	<xsl:template name="Cer1:tCertificationDoc">
		<xsl:element name="Certification_Doc">
			<xsl:element name="Organization">
				<xsl:value-of select="Cer1:Organization"/>
			</xsl:element>
			<xsl:element name="Date">
				<xsl:value-of select="Cer1:Date"/>
			</xsl:element>
			<xsl:element name="Number">
				<xsl:value-of select="Cer1:Number"/>
			</xsl:element>
			<xsl:element name="Appointment">
				<xsl:value-of select="Cer1:Official/Cer1:Appointment"/>
			</xsl:element>
			<xsl:element name="FIO">
				<xsl:value-of select="concat(Cer1:Official/ns7:FamilyName,' ',Cer1:Official/ns7:FirstName,' ',Cer1:Official/ns7:Patronymic)"/>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	
	<!--************************************************КАДАСТРОВЫЙ ПЛАН ТЕРРИТОРИИ*****************************************************-->
	<xsl:template name="Region_Cadastr">
		<xsl:variable name="vCodeType">
			<xsl:choose>
				<xsl:when test="eDocument">
					<xsl:value-of select="eDocument/@CodeType"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="@CodeType"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="vVersion">
			<xsl:choose>
				<xsl:when test="eDocument">
					<xsl:value-of select="eDocument/@Version"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="@Version"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="vDate">
			<xsl:value-of select="Package/Certification_Doc/Date"/>
		</xsl:variable>
		<xsl:element name="KPTdocs">
			<xsl:element name="KPT">
				<xsl:attribute name="CodeType"><xsl:value-of select="$vCodeType"/></xsl:attribute>
				<xsl:attribute name="Version"><xsl:value-of select="$vVersion"/></xsl:attribute>
				<xsl:attribute name="Name"><xsl:value-of select="//Cadastral_Block[1] /@CadastralNumber"/></xsl:attribute>
				<xsl:attribute name="Date"><xsl:value-of select="$vDate"/></xsl:attribute>
				<xsl:element name="SubItems">
					<xsl:apply-templates select="//Parcel" mode="Region_Cadastr"/>
				</xsl:element>
				<xsl:copy-of select="Package/Certification_Doc"/>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	<xsl:template match="Parcel" mode="Region_Cadastr">
		<xsl:element name="SubItem">
			<xsl:attribute name="Name"><xsl:call-template name="GetCadNum_ByParcel"/></xsl:attribute>
			<xsl:attribute name="Code"><xsl:text>Parcel</xsl:text></xsl:attribute>
		</xsl:element>
	</xsl:template>
	<!--09-->
	<xsl:template name="KPT09">
		<xsl:variable name="_vCodeType" select="'-'"/>
		<xsl:variable name="_vVersion" select="'09'"/>
		<xsl:variable name="_vName" select="kpt09:CadastralBlocks/kpt09:CadastralBlock[1]/@CadastralNumber"/>	
		<xsl:variable name="_vDate" select="kpt09:CertificationDoc/Cer1:Date"/>
		<xsl:element name="KPTdocs">
			<xsl:element name="KPT">
				<xsl:attribute name="CodeType"><xsl:value-of select="$_vCodeType"/></xsl:attribute>
				<xsl:attribute name="Version"><xsl:value-of select="$_vVersion"/></xsl:attribute>
				<xsl:attribute name="Name"><xsl:value-of select="$_vName"/></xsl:attribute>
				<xsl:attribute name="Date"><xsl:value-of select="$_vDate"/></xsl:attribute>
				<xsl:element name="SubItems">
					<xsl:apply-templates select="//kpt09:Parcel"/>
				</xsl:element>
				<xsl:apply-templates select="kpt09:CertificationDoc"/>
			</xsl:element>
		</xsl:element>		
	</xsl:template>
	<xsl:template match="kpt09:CertificationDoc">
		<xsl:call-template name="Cer1:tCertificationDoc"/>
	</xsl:template>
	<xsl:template match="kpt09:Parcel">
		<xsl:element name="SubItem">
			<xsl:attribute name="Name">
				<xsl:variable name="_vAccountNumber">
					<xsl:variable name="_vParentCadastralNumbers" select="kpt09:ParentCadastralNumbers/kpt09:CadastralNumber"/>
					<xsl:choose>
						<xsl:when test="$_vParentCadastralNumbers!=''">
							<xsl:value-of select="concat($_vParentCadastralNumbers,'(',@CadastralNumber,')')"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="@CadastralNumber"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<xsl:value-of select="$_vAccountNumber"/>
			</xsl:attribute>
			<xsl:attribute name="Code"><xsl:text>Parcel</xsl:text></xsl:attribute>
		</xsl:element>
	</xsl:template>
	<!--10-->
	<xsl:template name="KPT10">
		<xsl:variable name="_vCodeType" select="'-'"/>
		<xsl:variable name="_vVersion" select="'10'"/>
		<xsl:variable name="_vName" select="kpt10:CadastralBlocks/kpt10:CadastralBlock[1]/@CadastralNumber"/>	
		<xsl:variable name="_vDate" select="kpt10:CertificationDoc/Cer1:Date"/>
		<xsl:element name="KPTdocs">
			<xsl:element name="KPT">
				<xsl:attribute name="CodeType"><xsl:value-of select="$_vCodeType"/></xsl:attribute>
				<xsl:attribute name="Version"><xsl:value-of select="$_vVersion"/></xsl:attribute>
				<xsl:attribute name="Name"><xsl:value-of select="$_vName"/></xsl:attribute>
				<xsl:attribute name="Date"><xsl:value-of select="$_vDate"/></xsl:attribute>
				<xsl:element name="SubItems">
					<xsl:apply-templates select="//kpt10:Parcel"/>
				</xsl:element>
				<xsl:apply-templates select="kpt10:CertificationDoc"/>
			</xsl:element>
		</xsl:element>		
	</xsl:template>
	<xsl:template match="kpt10:CertificationDoc">
		<xsl:call-template name="Cer1:tCertificationDoc"/>
	</xsl:template>
	<xsl:template match="kpt10:Parcel">
		<xsl:element name="SubItem">
			<xsl:attribute name="Name">
				<xsl:variable name="_vAccountNumber">
					<xsl:variable name="_vParentCadastralNumbers" select="kpt10:ParentCadastralNumbers/kpt10:CadastralNumber"/>
					<xsl:choose>
						<xsl:when test="$_vParentCadastralNumbers!=''">
							<xsl:value-of select="concat($_vParentCadastralNumbers,'(',@CadastralNumber,')')"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="@CadastralNumber"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<xsl:value-of select="$_vAccountNumber"/>
			</xsl:attribute>
			<xsl:attribute name="Code"><xsl:text>Parcel</xsl:text></xsl:attribute>
		</xsl:element>
	</xsl:template>
	<!--************************************************КАДАСТРОВАЯ ВЫПИСКА*****************************************************-->
	<xsl:template name="Region_Cadastr_Vidimus_KV">
		<xsl:variable name="vCodeType">
			<xsl:value-of select="@CodeType"/>
		</xsl:variable>
		<xsl:variable name="vVersion">
			<xsl:value-of select="@Version"/>
		</xsl:variable>
		<xsl:variable name="vDate">
			<xsl:value-of select="Package/Certification_Doc/Date"/>
		</xsl:variable>
		<xsl:element name="KVdocs">
			<xsl:element name="KV">
				<xsl:attribute name="CodeType"><xsl:value-of select="$vCodeType"/></xsl:attribute>
				<xsl:attribute name="Version"><xsl:value-of select="$vVersion"/></xsl:attribute>
				<xsl:attribute name="Name"><xsl:apply-templates select="//Parcel" mode="Region_Cadastr_Vidimus_KV"/></xsl:attribute>
				<xsl:attribute name="Date"><xsl:value-of select="$vDate"/></xsl:attribute>
				<xsl:element name="SubItems"/>
				<xsl:copy-of select="Package/Certification_Doc"/>			
			</xsl:element>
			
		</xsl:element>
	</xsl:template>
	<xsl:template match="Parcel" mode="Region_Cadastr_Vidimus_KV">
		<xsl:call-template name="GetCadNum_ByParcel"/>
	</xsl:template>
	<!--06-->
	<xsl:template match="kvzu06:CertificationDoc">
		<xsl:call-template name="Cer1:tCertificationDoc"/>
	</xsl:template>
	<xsl:template name="KVZU06">
		<xsl:variable name="_vCodeType" select="'-'"/>
		<xsl:variable name="_vVersion" select="'06'"/>
		<xsl:variable name="_vName" select="kvzu06:Parcels/kvzu06:Parcel/@CadastralNumber"/>	
		<xsl:variable name="_vDate" select="kvzu06:CertificationDoc/Cer1:Date"/>	
		<xsl:element name="KVdocs">
			<xsl:element name="KV">
				<xsl:attribute name="CodeType"><xsl:value-of select="$_vCodeType"/></xsl:attribute>
				<xsl:attribute name="Version"><xsl:value-of select="$_vVersion"/></xsl:attribute>
				<xsl:attribute name="Name"><xsl:value-of select="$_vName"/></xsl:attribute>
				<xsl:attribute name="Date"><xsl:value-of select="$_vDate"/></xsl:attribute>
				<xsl:element name="SubItems"/>
				<xsl:apply-templates select="kvzu06:CertificationDoc"/>
			</xsl:element>	
		</xsl:element>
	</xsl:template>
	<!--07-->
	<xsl:template match="kvzu07:CertificationDoc">
		<xsl:call-template name="Cer1:tCertificationDoc"/>
	</xsl:template>
	<xsl:template name="KVZU07">
		<xsl:variable name="_vCodeType" select="'-'"/>
		<xsl:variable name="_vVersion" select="'06'"/>
		<xsl:variable name="_vName" select="kvzu07:Parcels/kvzu07:Parcel/@CadastralNumber"/>	
		<xsl:variable name="_vDate" select="kvzu07:CertificationDoc/Cer1:Date"/>	
		<xsl:element name="KVdocs">
			<xsl:element name="KV">
				<xsl:attribute name="CodeType"><xsl:value-of select="$_vCodeType"/></xsl:attribute>
				<xsl:attribute name="Version"><xsl:value-of select="$_vVersion"/></xsl:attribute>
				<xsl:attribute name="Name"><xsl:value-of select="$_vName"/></xsl:attribute>
				<xsl:attribute name="Date"><xsl:value-of select="$_vDate"/></xsl:attribute>
				<xsl:element name="SubItems"/>
				<xsl:apply-templates select="kvzu07:CertificationDoc"/>
			</xsl:element>	
		</xsl:element>
	</xsl:template>
	<!--************************************************КАДАСТРОВЫЙ ПАСПОРТ*****************************************************-->
	<xsl:template name="Region_Cadastr_Vidimus_KP">
		<xsl:variable name="vCodeType">
			<xsl:value-of select="@CodeType"/>
		</xsl:variable>
		<xsl:variable name="vVersion">
			<xsl:value-of select="@Version"/>
		</xsl:variable>
		<xsl:variable name="vDate">
			<xsl:value-of select="Package/Certification_Doc/Date"/>
		</xsl:variable>
		<xsl:element name="KPdocs">
			<xsl:element name="KP">
				<xsl:attribute name="CodeType"><xsl:value-of select="$vCodeType"/></xsl:attribute>
				<xsl:attribute name="Version"><xsl:value-of select="$vVersion"/></xsl:attribute>
				<xsl:attribute name="Name"><xsl:apply-templates select="//Parcel" mode="Region_Cadastr_Vidimus_KV"/></xsl:attribute>
				<xsl:attribute name="Date"><xsl:value-of select="$vDate"/></xsl:attribute>
				<xsl:element name="SubItems"/>
				<xsl:copy-of select="Package/Certification_Doc"/>		
			</xsl:element>
		</xsl:element>
	</xsl:template>
	<xsl:template match="Parcel" mode="Region_Cadastr_Vidimus_KP">
		<xsl:call-template name="GetCadNum_ByParcel"/>
	</xsl:template>	
	<!--05-->
	<xsl:template match="kpzu05:CertificationDoc">
		<xsl:call-template name="Cer1:tCertificationDoc"/>
	</xsl:template>
	<xsl:template name="KPZU05">
		<xsl:variable name="_vCodeType" select="'-'"/>
		<xsl:variable name="_vVersion" select="'05'"/>
		<xsl:variable name="_vName" select="kpzu05:Parcel/@CadastralNumber"/>	
		<xsl:variable name="_vDate" select="kpzu05:CertificationDoc/Cer1:Date"/>
		<xsl:element name="KPdocs">
			<xsl:element name="KP">
				<xsl:attribute name="CodeType"><xsl:value-of select="$_vCodeType"/></xsl:attribute>
				<xsl:attribute name="Version"><xsl:value-of select="$_vVersion"/></xsl:attribute>
				<xsl:attribute name="Name"><xsl:value-of select="concat($_vName,' КПЗУ')"/></xsl:attribute>
				<xsl:attribute name="Date"><xsl:value-of select="$_vDate"/></xsl:attribute>
				<xsl:element name="SubItems"/>
				<xsl:apply-templates select="kpzu05:CertificationDoc"/>
			</xsl:element>
		</xsl:element>		
	</xsl:template>
	<!--06-->
	<xsl:template match="kpzu06:CertificationDoc">
		<xsl:call-template name="Cer1:tCertificationDoc"/>
	</xsl:template>
	<xsl:template name="KPZU06">
		<xsl:variable name="_vCodeType" select="'-'"/>
		<xsl:variable name="_vVersion" select="'05'"/>
		<xsl:variable name="_vName" select="kpzu06:Parcel/@CadastralNumber"/>	
		<xsl:variable name="_vDate" select="kpzu06:CertificationDoc/Cer1:Date"/>
		<xsl:element name="KPdocs">
			<xsl:element name="KP">
				<xsl:attribute name="CodeType"><xsl:value-of select="$_vCodeType"/></xsl:attribute>
				<xsl:attribute name="Version"><xsl:value-of select="$_vVersion"/></xsl:attribute>
				<xsl:attribute name="Name"><xsl:value-of select="concat($_vName,' КПЗУ')"/></xsl:attribute>
				<xsl:attribute name="Date"><xsl:value-of select="$_vDate"/></xsl:attribute>
				<xsl:element name="SubItems"/>
				<xsl:apply-templates select="kpzu06:CertificationDoc"/>
			</xsl:element>
		</xsl:element>		
	</xsl:template>
	<!--***************************************************************ЕГРП***************************************************************-->
	<xsl:template name="Extract">
		<xsl:variable name="vCodeType">
			<xsl:value-of select="@CodeType"/>
		</xsl:variable>
		<xsl:variable name="vVersion">
			<xsl:value-of select="ReestrExtract/DeclarAttribute/@Version"/>
		</xsl:variable>
		<xsl:variable name="vDate">
			<xsl:value-of select="ReestrExtract/DeclarAttribute/@ExtractDate"/>
		</xsl:variable>
		<xsl:element name="dddddd"/>
		<xsl:element name="EGRPdocs">
			<xsl:element name="EGRP">
				<xsl:attribute name="CodeType"><xsl:value-of select="$vCodeType"/></xsl:attribute>
				<xsl:attribute name="Version"><xsl:value-of select="$vVersion"/></xsl:attribute>
				<xsl:attribute name="Name">
					<xsl:value-of select="ReestrExtract/ExtractObjectRight/ExtractObject/ObjectRight/ObjectDesc/CadastralNumber"/>
				</xsl:attribute>
				<xsl:attribute name="Date"><xsl:value-of select="$vDate"/></xsl:attribute>
				<xsl:element name="SubItems"/>
				<xsl:element name="Certification_Doc">
					<xsl:element name="Organization"/>
					<xsl:element name="Date">
						<xsl:value-of select="$vDate"/>
					</xsl:element>
					<xsl:element name="Number">
						<xsl:value-of select="ReestrExtract/DeclarAttribute/@ExtractNumber"/>
					</xsl:element>
					<xsl:element name="Appointment"/>
					<xsl:element name="FIO"/>			
				</xsl:element>			
			</xsl:element>
		</xsl:element>
	</xsl:template>
	<!--************************************************КАДАСТРОВЫЙ ПАСПОРТ ОКС*****************************************************-->
	<xsl:template match="kpoks03:CertificationDoc">
		<xsl:call-template name="Cer1:tCertificationDoc"/>
	</xsl:template>
	<xsl:template name="KPOKS03">
		<xsl:variable name="_vCodeType" select="'-'"/>
		<xsl:variable name="_vVersion" select="'03'"/>
		<xsl:variable name="_vName" select="kpoks03:Realty//@CadastralNumber[1]"/>	
		<xsl:variable name="_vDate" select="kpoks03:CertificationDoc/Cer1:Date"/>
		<xsl:element name="KPdocs">
			<xsl:element name="KP">
				<xsl:attribute name="CodeType"><xsl:value-of select="$_vCodeType"/></xsl:attribute>
				<xsl:attribute name="Version"><xsl:value-of select="$_vVersion"/></xsl:attribute>
				<xsl:attribute name="Name"><xsl:value-of select="concat($_vName,' КПОКС')"/></xsl:attribute>
				<xsl:attribute name="Date"><xsl:value-of select="$_vDate"/></xsl:attribute>
				<xsl:element name="SubItems"/>
				<xsl:apply-templates select="kpoks03:CertificationDoc"/>
			</xsl:element>
		</xsl:element>		
	</xsl:template>

</xsl:stylesheet>
