{*******************************************************}
{      |----------------------------------------|       }
{      |-(c) 2009-2013 ������������ ����������.-|       }
{      |-          ��������� ������.           -|       }
{      |-        ���������� ���������.         -|       }
{      |----------------------------------------|       }
{*******************************************************}

unit MSStringParser;

interface
 uses System.SysUtils;
type

TMSStringParser = class
private
 parsed : array of string;
 delimiterstring : string;
public
 constructor Create; overload;
 constructor Create(initialdelimiters : string); overload;
 destructor Destroy; override;
 function Parse(src : string; SkipEmpty:boolean = true) : integer;
 function GetToken(index : integer) : string;
 procedure SetToken(index : integer; newtok : string);
 function GetCountToken: integer;
 function GetString(delim : char) : string;
 function FindToken(what : string) : integer;
 property Delimiters : string read delimiterstring write delimiterstring;
end;

TMIDLineParser = class(TMSStringParser)
 function  ParseMIDLine(src : string) : integer;
end;

function Tab : char;


implementation

{ TStringParser }

constructor TMSStringParser.Create;
begin
 SetLength(parsed, 1);
 delimiterstring := #9;
end;

constructor TMSStringParser.Create(initialdelimiters: string);
begin
 SetLength(parsed, 1);
 delimiterstring := initialdelimiters;
end;

destructor TMSStringParser.Destroy;
begin
 SetLength(parsed, 0);
end;

function TMSStringParser.FindToken(what: string): integer;
var
 count : integer;
 found : integer;
begin
 found := -1;
 for count := 0 to Length(parsed) - 1 do
  if parsed[count] = what then
   found := count;
 Result := found;
end;

function TMSStringParser.GetCountToken: integer;
begin
 result:= length(self.parsed);
end;

function TMSStringParser.GetString(delim: char): string;
var
 tokcount : integer;
 buffer : string;
begin
 buffer := '';
 for tokcount := 0 to Length(parsed) - 1 do
  buffer := buffer + delim + parsed[tokcount];
  Result := buffer;
end;

function TMSStringParser.GetToken(index: integer): string;
begin
 if index < Length(parsed) then
  Result := parsed[index]
 else
  Result := 'ERROR';
end;

function TMSStringParser.Parse(src: string; SkipEmpty:boolean = true): integer;
var
 _i : integer;
 _NewItem: boolean;
begin
 _NewItem:= false;
 SetLength(parsed, 0);
 for _i := 1 to Length(src) do begin
  if Pos(src[_i], delimiterstring) <> 0 then begin //������� ������ �������� ������������
   if _NewItem and  not SkipEmpty then //�������� �������� ���������� �����������
    SetLength(parsed, length(parsed)+1);
   _NewItem:= true;
  end else begin
   if _NewItem or (Length(parsed)=0) then begin
    SetLength(parsed, length(parsed)+1);
    _NewItem:= false;
   end;
  parsed[high(parsed)]:= parsed[high(parsed)] + src[_i];
 end;
end;
 Result := self.GetCountToken;
end;

function Tab : char;
begin
 Result := #9;
end;

procedure TMSStringParser.SetToken(index: integer; newtok: string);
begin
 if index < Length(parsed) then
  parsed[index] := newtok;
end;

{ TMIDLineParser }

function TMIDLineParser.ParseMIDLine(src: string): integer;
const
 numchar = ['0'..'9','.','-'];
var
 linedelimiter : char;
 count : integer;

function ReadString(startindex : integer) : string;
var
 dcount : integer;
 s : string;
begin
 s := '';
 dcount := startindex + 1;
 while (src[dcount] <> '"') or((src[dcount] = '"')and(dcount<length(src)) and (src[dcount+1]<>linedelimiter) and
       (src[dcount+1]<>#0) and (src[dcount+1]<>#10) and (src[dcount+1]<>#13)) do begin
  s := s + src[dcount];
  inc(dcount);
 end;
 Result := s;
 count := dcount + 2;
end;

function ReadNumber(startindex : integer) : string;
var
 dcount : integer;
 s : string;
begin
 s := '';
 dcount := startindex;
 while src[dcount] in numchar do
 begin
  s := s + src[dcount];
  inc(dcount);
 end;
 Result := s;
 count := dcount + 1;
end;

function CheckString(startindex : integer) : boolean;
begin
  if src[startindex] = '"' then
   Result := true
  else
   Result := false;
end;

function CheckDelimiter(startindex : integer) : boolean;
begin
  if src[startindex] = linedelimiter then
   Result := true
  else
   Result := false;
end;

function CheckBoolean(startindex:integer) : boolean;
begin
 if UpCase(src[startindex]) in ['T','F'] then
   Result := true
  else
   Result := false;
end;


begin
  count := 1;
  SetLength(parsed, 0);

 { linedelimiter := src[1];

  if src[1] in numchar then
  begin
   ReadNumber(1);
   linedelimiter := src[count + 1];
  end;

  if CheckString(1) then
  begin
   ReadString(1);
   linedelimiter := src[count - 1];
  end;}

  linedelimiter := self.delimiterstring[1];

  count := 1;
  while count <= length(src) do
  begin

    if CheckDelimiter(count) then
    begin
     SetLength(parsed, length(parsed) + 1);
     parsed[length(parsed) - 1] := '';
     count := count + 1;
     continue;
    end;

    if CheckString(count) then begin
     SetLength(parsed, length(parsed) + 1);
     parsed[length(parsed) - 1] := ReadString(count);
    end else
     if CheckBoolean(count) then begin
      SetLength(parsed, length(parsed) + 1);
      parsed[length(parsed) - 1] := BoolToStr(src[count] = 'T');
      count := count + 2;
     end else begin
      SetLength(parsed, length(parsed) + 1);
      parsed[length(parsed) - 1] := ReadNumber(count);
     end;


  end;
  Result := Length(parsed);
end;


end.
