unit KPT;

interface

uses
  Winapi.Windows, Winapi.Messages, WinAPI.ShellAPI,
  System.SysUtils, System.Variants, System.Classes, System.IOUtils,
  System.Types, System.UITypes, System.Actions,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Buttons,
  Vcl.ExtCtrls, Vcl.ComCtrls, Vcl.CheckLst, Vcl.Menus, Vcl.ActnList,
  Vcl.Imaging.pngimage,
  XmlBase.DataBase.Intf;

type
  TForm1 = class(TForm)
    MenuUpload: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    N5: TMenuItem;

    actlst: TActionList;
    actDoFind: TAction;
    actDoFindMask: TAction;

    PageControl1: TPageControl;
    TabSearch: TTabSheet;
    TbMask: TTabSheet;
    TabLoad: TTabSheet;
    TabAbout: TTabSheet;
    // �������� ����
    pnlSearch: TPanel;
    spbtnBack: TSpeedButton;
    EdtSearch: TEdit;

    CheckSearchFile: TCheckListBox;
    spl3: TSplitter;
    MemoInf: TMemo;
    pnlUpload: TPanel;
    lblUpload: TLabel;
    CheckZIP: TCheckBox;
    CheckXML: TCheckBox;
    btnUpload: TBitBtn;
    // ����� xml
    pnlMask: TPanel;
    MemoMask: TMemo;
    spbtnSearch: TSpeedButton;
    spl1: TSplitter;
    pnl1: TPanel;
    spl2: TSplitter;
    MemoSearchMask: TMemo;
    lbl1: TLabel;
    chBoxMZip: TCheckBox;
    chBoxMXml: TCheckBox;
    btnUploadMask: TBitBtn;
    BtnBaseDir: TBitBtn;
    lstLoad: TListBox;
    CheckSearchMask: TCheckListBox;
    // About
    Panel2: TPanel;
    Version: TLabel;
    Copyright: TLabel;
    ProgramIcon: TImage;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    OpenDialog1: TOpenDialog;


    procedure BtnBaseDirClick(Sender: TObject);
    procedure N1Click(Sender: TObject);
    procedure N2Click(Sender: TObject);
    procedure N3Click(Sender: TObject);
    procedure btnUploadClick(Sender: TObject);

    procedure actDoFindExecute(Sender: TObject);
    procedure actDoFindMaskExecute(Sender: TObject);

    procedure CheckSearchFileDblClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdtSearchChange(Sender: TObject);
    procedure Label3Click(Sender: TObject);
    procedure spbtnBackClick(Sender: TObject);
    procedure spbtnSearchClick(Sender: TObject);


    procedure PageControl1Change(Sender: TObject);
    procedure N4Click(Sender: TObject);
    procedure CheckSearchMaskDblClick(Sender: TObject);
    procedure btnUploadMaskClick(Sender: TObject);
  private
    { Private declarations }
    FDataBase:IDataBase;
    procedure ApplySetting;

  public
    { Public declarations }
    procedure ShowInformation(Check:TCheckListBox; Memo:TMemo);

  end;

var
  Form1: TForm1;

implementation

uses
  MSStringParser, Mgis.UI.SelectDirectory;

resourcestring
  rsMaskZIP = '*.zip';
  rsMaskXML = '*.xml';
  rsMaskTXT = '*.txt';
  rsTempConvertDirName = 'TempConvert';

  {$R *.dfm}

function _FilterPredicate(const Path: string; const SearchRec: TSearchRec): Boolean;
begin
    Result:= TPath.GetExtension(rsMaskXML) = tpath.GetExtension(SearchRec.Name);
    Result:= Result or (TPath.GetExtension(rsMaskZIP) = tpath.GetExtension(SearchRec.Name));
end;



procedure TForm1.ShowInformation(Check:TCheckListBox; Memo:TMemo);
var
  _i:integer;
begin
 Memo.Clear;
 for _i := 0 to Check.Count-1  do
  if Check.Selected[_i] or Check.Checked[_i] then
    Memo.Lines.Add(self.FDataBase.ShowInformation(self.FDataBase.FinedItems[_i]));
end;

procedure TForm1.CheckSearchFileDblClick(Sender: TObject);
begin
 if self.CheckSearchFile.ItemIndex=-1 then EXIT;
 self.EdtSearch.Text:= self.CheckSearchFile.Items[self.CheckSearchFile.ItemIndex];
 self.actDoFind.Execute;
end;

procedure TForm1.CheckSearchMaskDblClick(Sender: TObject);
begin
  CheckSearchMask.Checked[CheckSearchMask.ItemIndex]:= NOT CheckSearchMask.Checked[CheckSearchMask.ItemIndex];
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
 self.ApplySetting;
 self.actDoFind.Execute;
end;

procedure TForm1.actDoFindExecute(Sender: TObject);
var
 _i: integer;
begin
 self.MemoInf.Clear;
 self.FDataBase.FindbyNumList(self.EdtSearch.Text);
 if Length(self.FDataBase.FinedItems)=0 then EXIT;

 self.CheckSearchFile.Items.BeginUpdate;
 TRY
 self.CheckSearchFile.Items.Clear;
 for _i :=0 to High(self.FDataBase.FinedItems) do
  self.CheckSearchFile.Items.Add(TPath.GetFileName(self.FDataBase.FinedItems[_i]));
 FINALLY
  self.CheckSearchFile.Items.EndUpdate;
 END;
end;

procedure TForm1.ApplySetting;
var
  DLL_XMLBase_Factory: function(aPathCnfg:WideString): IDataBase; stdcall;
  LibHandle:THandle;
begin
 if self.FDataBase=nil then
  begin
    LibHandle := LoadLibrary('XMLBaseDLL.DLL');
    if LibHandle >= 32 then
    begin
      //    �������� ����� ������� � ����������
      DLL_XMLBase_Factory := GetProcAddress(LibHandle, 'XMLDataBaseFactory');
      if Assigned(DLL_XMLBase_Factory) then
        Self.FDataBase:=DLL_XMLBase_Factory(TPath.GetDirectoryName(Application.ExeName))
      else
        raise Exception.Create('�� ������� ��������� ������� �� ����������.');
    end;
  end;
end;

procedure TForm1.BtnBaseDirClick(Sender: TObject);
var
  _i:integer;
  _stb:TStringBuilder;
begin
  if OpenDialog1.Execute then begin
  // �������������� ��������� �����
  if TDirectory.Exists(self.FDataBase.TempDirecroty) then
    TDirectory.Delete(self.FDataBase.TempDirecroty, TRUE);
  TDirectory.CreateDirectory(self.FDataBase.TempDirecroty);
  _stb:= TStringBuilder.Create;
  for _i:= 0 to OpenDialog1.Files.Count-1 do
  begin
    TRY
      self.FDataBase.Load(OpenDialog1.Files[_i]);
      lstLoad.Items.Add('���� '+TPath.GetFileName(OpenDialog1.Files[_i])+' ��������.');
    EXCEPT
      _stb.AppendLine(OpenDialog1.Files[_i]);
      lstLoad.Items.Add('���� '+TPath.GetFileName(OpenDialog1.Files[_i])+' ������!');
    END;
    Application.ProcessMessages;
  end;
  if _stb.Length<>0 then
    MessageDlg('������ ����� �� ���� ���������: '+sLineBreak+_stb.ToString,mtError,[mbOK],-1)
  else
    MessageDlg('�������� ��������� �������.',mtInformation,[mbOK],-1);
  _stb.Free;
end;

end;

// ______________ ����� �� ���� ________________________________________________
// �������� ����������� ����
procedure TForm1.N1Click(Sender: TObject);
begin
  CheckSearchFile.CheckAll(cbChecked);
  CheckSearchMask.CheckAll(cbChecked);
end;

procedure TForm1.N2Click(Sender: TObject);
begin
  CheckSearchFile.CheckAll(cbUnchecked);
  CheckSearchMask.CheckAll(cbUnchecked);
end;

procedure TForm1.N3Click(Sender: TObject);
begin
  //CheckSearchFile.Items.Text:=SearchFiles(LbLSearch.Text);
  self.actDoFind.Execute;
  self.actDoFindMask.Execute;
end;

procedure TForm1.N4Click(Sender: TObject);
begin
  case PageControl1.TabIndex of
    0:ShowInformation(CheckSearchFile, MemoInf);
    1:ShowInformation(CheckSearchMask, MemoSearchMask);
  end;

end;

procedure TForm1.PageControl1Change(Sender: TObject);
begin
  EdtSearch.Clear;
  MemoMask.Clear;
  CheckSearchMask.Clear;
  MemoSearchMask.Clear;
  lstLoad.Clear;
end;

procedure TForm1.spbtnBackClick(Sender: TObject);
var
  _parse:TMSStringParser;
  _index:integer;
  _str:string;
begin
  MemoInf.Clear;
  _parse:=TMSStringParser.Create(' ;:');
  _parse.Parse(self.EdtSearch.Text);

  for _index := 0 to _parse.GetCountToken-2 do
    _str:=_str+_parse.GetToken(_index)+' ';
  self.EdtSearch.Text:=TRIM(_str);
  _parse.Free;
  self.actDoFind.Execute;
end;

procedure TForm1.EdtSearchChange(Sender: TObject);
begin
 self.actDoFind.Execute;
end;

// ����� �� ��������
procedure TForm1.Label3Click(Sender: TObject);
begin
  ShellExecute(handle, 'open', 'http://www.comp-tech.ru/', nil, nil, SW_SHOW);
end;

procedure TForm1.btnUploadClick(Sender: TObject);
var
 _i: integer;
 _fold:String;
begin
 if not ExecuteDlgSelDirectory(_fold) then EXIT;
 self.FDataBase.DestDirectory:= _fold;

 if self.CheckXML.Checked then
   self.FDataBase.AddKind(TItemKind.dbiXML);
 if self.CheckZIP.Checked then
    self.FDataBase.AddKind(TItemKind.dbiZIP);
 if self.FDataBase.Kind=[] then
  raise Exception.Create('���������� ������� ��� ������ ��� ��������.');

 for _I := 0 to self.CheckSearchFile.Items.Count-1 do
  if self.CheckSearchFile.Checked[_I] then
    self.FDataBase.UpLoad(_I);
 MessageDlg('�������� ��������� �������',mtInformation,[mbOK],-1);
end;


procedure TForm1.btnUploadMaskClick(Sender: TObject);
var
  _i: integer;
  _fold:String;
begin
 if not ExecuteDlgSelDirectory(_fold) then EXIT;
  self.FDataBase.DestDirectory:= _fold;
  if self.chBoxMXml.Checked then
    self.FDataBase.AddKind(TItemKind.dbiXML);
  if self.chBoxMZip.Checked then
    self.FDataBase.AddKind(TItemKind.dbiZIP);
  if self.FDataBase.Kind=[] then
    raise Exception.Create('���������� ������� ��� ������ ��� ��������.');

 for _I := 0 to self.CheckSearchMask.Items.Count-1 do
  if self.CheckSearchMask.Checked[_i] then
    self.FDataBase.UpLoadMask(_I);
 MessageDlg('�������� ��������� �������',mtInformation,[mbOK],-1);
end;

// --------------- ������ � ������� �� ����� -----------------------------------
procedure TForm1.actDoFindMaskExecute(Sender: TObject);
var
  _i:integer;
  _st: string;
begin
 self.MemoSearchMask.Clear;
 self.FDataBase.FindByMaskList(self.MemoMask.Text);
 self.CheckSearchMask.Clear;
 if Length(self.FDataBase.FinedItems)=0 then
 begin
    MemoSearchMask.Text:='����� �� ��� �����������!';
  EXIT;
 end;
 self.CheckSearchMask.Items.BeginUpdate;
 TRY
  self.CheckSearchMask.Items.Clear;
  for _i :=0 to High(self.FDataBase.FinedItems) do
  begin
    self.CheckSearchMask.Items.Add(TPath.GetFileName(self.FDataBase.FinedItems[_i]));
    Application.ProcessMessages;
  end;
  self.MemoSearchMask.Lines.Add('--�� �������--');
  for _st in self.FDataBase.NoFined do
     self.MemoSearchMask.Lines.Add(_st);

 FINALLY
    self.CheckSearchMask.Items.EndUpdate;
 END;
end;

procedure TForm1.spbtnSearchClick(Sender: TObject);
begin
  MemoSearchMask.Clear;
  Self.actDoFindMask.Execute;
end;
end.
