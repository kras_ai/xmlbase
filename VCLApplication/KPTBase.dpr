program KPTBase;

uses
  Vcl.Forms,
  System.IOUtils,
  KPT in 'KPT.pas' {Form1},
  Vcl.Themes,
  Vcl.Styles,
  XmlBase.DataBase.Intf in '..\XmlBaseCore\XmlBase.DataBase.Intf.pas',
  MSStringParser in '..\XmlBaseCore\MSStringParser.pas',
  Mgis.UI.SelectDirectory in '..\XmlBaseCore\Mgis.UI.SelectDirectory.pas';

{$R *.res}

begin
  Application.Initialize;



  Application.MainFormOnTaskbar := True;
  TStyleManager.TrySetStyle('Cobalt XEMedia');
  Application.CreateForm(TForm1, Form1);
  Application.Run;
end.
